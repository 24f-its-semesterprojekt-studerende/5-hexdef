# Dokumentation for eksamensprojektet

## Screenshots

### Firewalls 
**Overblik over de forskellige netværks firewalls og deres individuelle regelsæt i OPNsense-routeren på gruppens Proxmox-server:**

**01MGT - 10.35.10.0/24:**

![alt text](billeder/01MGT_Firewall.png)

**02DMZ - 10.35.20.0/24:**

![alt text](billeder/02DMZ_Firewall.png)

**03MONITOR - 10.35.30.0/24:**

![alt text](billeder/03MONITOR_Firewall.png)

**04ADMINISTRATION - 10.35.40.0/24:**

![alt text](billeder/04ADMINISTRATION_Firewall.png)

## Diagrammer/Tabeller/Modeller
**Organisationsdiagram for den fiktive virksomhed "Energiva":**

![alt text](billeder/Organisationsdiagram_Energiva.png)

**Mitre Attack Framework's Enterprise Matrice:**

![alt text](billeder/Enterprise_matrice.png)

**Netværksdiagram før segmentering:**

![alt text](billeder/Netværksdiagram_fladt1.png)

**Netværksdiagram efter segmentering:**

![alt text](billeder/Netværksdiagram_Segmenteret2.png)

## Projektplan

Gruppens brainstorm samt projektplan for eksamensprojektet blev lavet i følgende google docs dokument:
[Projektplan](https://docs.google.com/document/d/1Xf0IM-dMKxBoB2UnZUkgx-LQ6B1FLMNKkL7MYGkpr_w/edit?usp=sharing)

### Tidsplan
![alt text](billeder/tidsplan.png)
