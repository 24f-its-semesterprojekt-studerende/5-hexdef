## Introduktion til IT-Sikkerhed Uge 7  

[Undervisningsplan uge 7](https://ucl-pba-its.gitlab.io/24f-its-intro/weekly/ww_02/)  

### Øvelse 28 - CIA modellen  
Confidentiality, Integrity & Availability 


Øvelsen skal laves som gruppe/team

* Læs om CIA modellen i bogen "IT-Sikkerhed i praksis, en introduktion" kapitel 2.
* Vælg et af følgende scenarier som i vil vurdere i forhold til CIA:
    * En password manager (software)
    * En lægeklinik (virksomhed)
    * Dine data på computere og cloud (...)
    * Energileverandør (kritisk infrastruktur)
* Vurder, prioitér scenariet i forhold til CIA modellen. Noter og begrund jeres valg og overvejelser.
* Hvilke(n) type hacker angreb vil være mest aktuelt at tage forholdsregler imod? (DDos, ransomware, etc.)
* Hvilke forholdsregler kan i tage for at opretholde CIA i jeres scenarie (kryptering, adgangskontrol, hashing, logging etc.)
* Hver gruppe fremlægger det de er kommet frem til og alle giver feedback.





### Øvelse 2 - Gitlab & SSH nøgler  
[Øvelse 2](https://ucl-pba-its.gitlab.io/24f-its-intro/exercises/2_intro_opgave_git_gitlab_ssh/)

Vi fulgte denne [guide](https://docs.gitlab.com/ee/user/ssh.html)

Generer ssh key:
ssh-keygen -t ed25519 -C "comment"

Tilføj SSH til gitlab: ok
https://docs.gitlab.com/ee/user/ssh.html#add-an-ssh-key-to-your-gitlab-account

Verificer forbindelsen: 
https://docs.gitlab.com/ee/user/ssh.html#verify-that-you-can-connect



### THM:  

[Introductory networking](https://tryhackme.com/room/introtonetworking):  

[What is networking?](https://tryhackme.com/room/whatisnetworking):  

[Intro to LAN:](https://tryhackme.com/room/introtolan)  