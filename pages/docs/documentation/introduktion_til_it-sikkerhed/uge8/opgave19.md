https://ucl-pba-its.gitlab.io/24f-its-intro/exercises/19_intro_opgave_vmware_vsrx_3/


Øvelse 19 - konfigurer vsrx password og navn¶

Information¶
Af åbenlyse grunde er det god praksis, som minimum at give din router et password, det er også god praksis at navngive routere.
 Det skal du gøre i denne øvelse. Juniper vsrx er baseret på linux og det er faktisk linux du først møder  når du logger på med root. For at konfigurere routeren skal du logge på  junos som er junipers styresystem. Det gøres med kommandoen cli
![alt text](Billeder/networkdiagram2.png)
 Netværksdiagram

Instruktioner¶
1. login på vsrx med root og derefter start til junos med cli
2. skriv edit for at komme i konfiguration mode
3. skriv edit system root-authentication (tab completion kan bruges til kommandoer)
4. skriv set plain-text-password dit password skal være minimum 6 karakterer og må ikke kun være tal, jeg mener også der skal være et stort bogstav. Du vil se en fejlmeddelse hvis du ikke overholder password politikken. Gentag indtil dit password er accepteret.
5. skriv commit for at gemme ændringer, vent på commit complete i terminalen
6. skriv exit flere gange for at komme til login igen
7. login med root og dit nye password (husk at skrive det ned til senere brug!)
8. for at give routeren et navn, gå i edit mode igen
9. skriv set system host-name <navn> og derefter commit.
10. Kontroller navnet ved at skrive show system host-name

Det hele giver mening, så det var ez