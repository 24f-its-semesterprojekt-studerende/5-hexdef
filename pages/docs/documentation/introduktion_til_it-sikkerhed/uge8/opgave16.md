https://ucl-pba-its.gitlab.io/24f-its-intro/exercises/16_intro_opgave_vmware_xubuntu/

Instruktioner¶
1. Lav 2 xubuntu VM maskiner i vmware, brug xubuntu 20.04 LTS som image.
2. Xubuntu image download https://xubuntu.org/download/
3. hjælp til at installere en ny vm https://kb.vmware.com/s/article/1018415
4. Konfigurer de 2 linux maskiner's netværk ifølge netværks diagrammet
 ![config](Billeder/networkconfig_gateway.png)  

+ ![gateway](Billeder/networkconfig.png) i gateway'en
Gør det samme for den anden, men bare med 192.168.11.2

6. åbn vm indstillinger for hver xubuntu maskine. Sæt xubuntu 1 til vmnet1 og xubuntu 2 til vmnet2 (hvis du ikke har vmnet2 så lav det i vmware virtual network editor)