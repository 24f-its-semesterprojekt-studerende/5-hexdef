https://ucl-pba-its.gitlab.io/24f-its-intro/exercises/20_intro_opgave_vmware_vsrx_4/


Øvelse 20 - konfigurer vsrx routing¶

Information¶
Nu er det tid til at konfigurere vsrx til routing mellem de 2 xubuntu  maskiner, husk at kigge på netværksdiagrammet når du arbejder.
![alt text](Billeder/networkdiagram2.png)
 Netværksdiagram

Instruktioner¶
1. i edit mode skriv edit interfaces
2. konfigurer ge-0/0//1 til at bruge 192.168.10.0/24 netværket med kommandoen
set ge-0/0/1 unit 0 family inet address 192.168.10.1/24
3. konfigurer ge-0/0//2 til at bruge 192.168.11.0/24 netværket med kommandoen set ge-0/0/2 unit 0 family inet address 192.168.11.1/24
4. gem ændringer med commit
5. check routing table med run show route terse, output skal se ud som:
inet.0: 4 destinations, 4 routes (4 active, 0 holddown, 0 hidden)
+ = Active Route, - = Last Active, * = Both

A Destination        P Prf   Metric 1   Metric 2  Next hop         AS path
* 192.168.10.0/24    D   0                       >ge-0/0/1.0
* 192.168.10.1/32    L   0                        Local
* 192.168.11.0/24    D   0                       >ge-0/0/2.0
* 192.168.11.1/32    L   0                        Local

Det gjorde den 

1. skriv top for at komme ud af edit interfaces
2. skriv edit security zones for at configurere de 2 interfaces så de begge er i trust zonen
3. inkluder ge-0/0/1 i trust zonen med set security-zone trust interfaces ge-0/0/1 host-inbound-traffic system-services ping
4. inkluder ge-0/0/2 i trust zonen med set security-zone trust interfaces ge-0/0/2 host-inbound-traffic system-services ping
5. fjern untrust zonen delete security-zone untrust
6. genetabler untrust til default med set security-zone untrust
7. gem ændringer med commit
8. skriv show for at se security zones konfigurationen
![security zone config](securityzoneconfig.png)   
9. kontroller at dit ntævrk virker ved at pinge fra den ene computer til den anden, ping også de 2 router interfaces fra begge computere.

Det fungerede ikke, så 
![alt text](Billeder/vneditor2.png)
Jeg havde glemt at slå DHCP & Connect a host virtual adapter to this network fra på vmnet 1,2 & 3

![ping](Billeder/ping.png)

