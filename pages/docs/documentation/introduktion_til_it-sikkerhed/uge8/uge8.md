https://ucl-pba-its.gitlab.io/24f-its-intro/weekly/ww_03/

Uge 8 - Sikkerhed i netværksprotokoller¶
Mål for ugen¶

Denne uge omhandler netværk og sikkerhed i netværk. Emnerne belyses ved dels at arbejde med Wireshark for at lære at analysere mistænkelig netværkstrafik, dels ved at opsætte et lille netværk i vmware. Vmware netværket udvides i næste uge med installation af en sårbar webapplikation. Målet med netværket er at den studerende har et lukket miljø til at træne i værktøjer der kan bruges i sikkerheds arbejde. Endelig er der en Tryhackme øvelse der introducerer netværks scannings værktøjet NMAP.

Der er en del arbejde i dagens øvelser og det er derfor vigtigt at i bruger alt den tid i har til rådighed i dag.
Forberedelse¶

    Læs undervisningsplanen og øvelser
    Læs kapitel 4 – Beskyttelse af it-systemer, i "IT-Sikkerhed i praksis", hav fokus på netværk.
    Se CTF video hvor wireshark bruges https://youtu.be/A4_DOr7Eiqo

Praktiske mål¶

    2 xubuntu maskiner oprettet og konfigureret i vmware
    1 vsrx konfigureret i vmware

Øvelser¶

    Øvelse 26 - Wireshark analyse
    Øvelse 16 - Opsætning af virtuelle maskiner i vmware
    Øvelse 17 - Opsætning af vsrx virtuel router
    Øvelse 18 - Seriel forbindelse til vsrx virtuel router
    Øvelse 19 - konfigurer vsrx password og navn
    Øvelse 20 - konfigurer vsrx routing
    THM: NMAP
    THM: Wireshark - the basics

Læringsmål der arbejdes med i faget denne uge¶
Overordnede læringsmål fra studie ordningen¶

    Den studerende har viden om og forståelse for:
        Grundlæggende netværksprotokoller
        Sikkerhedsniveau i de mest anvendte netværksprotokoller

    Den studerende kan supportere løsning af sikkerhedsarbejde ved at:
        Opsætte et simpelt netværk.
        Mestre forskellige netværksanalyse tools

Læringsmål den studerende kan bruge til selvvurdering¶

    Den studerende kan anvende nmap til at skanne et netværk og wireshark til at sniffe netværkstrafik
    Den studerende kan i et virtuelt miljø (vmware workstation) opsætte netværksenheder på commandline niveau (Juniper vsrx) og konfigurere netværk på Linux hosts (xubuntu)
