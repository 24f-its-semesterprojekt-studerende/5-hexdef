## CTF
Vi startede med at prøve at bruge "Wifite", men da det ikke virkede ordenligt for os brugte vi i stedet airgeddon til denne opgave.
Vi brugte rockyou.txt til at dekryptere det handshake vi fik lavet med airgeddon.
Dernæst loggede vi ind på routeren og fik at vide at det var default credentials. 
Vi fandt passwordet på reddit "Netcompany123"
Vi ændrede derefter 5ghz netværkets SSID til "HexDef"
![alt text](Billeder/CTF.png)