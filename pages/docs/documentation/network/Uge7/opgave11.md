# Øvelse 11 - OSI modellen og enheder

### OSI modellen:

**Fysisk lag (Physical Layer):**

Elektriske, mekaniske og funktionelle egenskaber ved fysiske forbindelser.

Kabler, stik, signaltyper, fysiske topologier osv.

**Link lag (Data Link Layer):**

Dette lag faciliterer pålidelig dataoverførsel over den fysiske forbindelse.

Adressering, fejlkontrol, adgangskontrol.

Ethernet, WiFi (802.11), ARP (Address Resolution Protocol) osv.

**Netværkslag (Network Layer):**

Routing af data gennem netværket.

IP-adressering, routingtabeller, pakkerouting.

IP-protokollen, ICMP (Internet Control Message Protocol) osv.

**Transportlag (Transport Layer):**

Sikrer pålidelig dataoverførsel mellem slutpunkter.

Segmentering og reassembly, flow control, fejlkontrol.

TCP (Transmission Control Protocol), UDP (User Datagram Protocol) osv.

**Sessionlag (Session Layer):**

Etablering, styring og afslutning af sessions mellem enheder.

Sessionstyring, dialogkontrol.

NetBIOS, RPC (Remote Procedure Call) osv.

**Præsentationslag (Presentation Layer):**

Omhandler datarepræsentation og -kodning.

Datakonvertering, kryptering, kompression.

JPEG, GIF, SSL/TLS osv.

**Applikationslag (Application Layer):**

Brugerinteraktion, applikationsprotokoller.

E-mail (SMTP, POP3), web (HTTP, HTTPS), FTP (File Transfer Protocol) osv
![Osi modellen](original-seven-layers-of-osi-model-1627523878-JYjV8oybcC.png)


1. **Gennemfør i fællesskab tryhackme rummet [OSI model](https://tryhackme.com/room/osimodelzi)**

    Screenshot:
    ![alt text](OSI_THM.png)

2. **Find almindelige (consumer) eksempler på hardware enheder, og placer dem på OSI modellen.** 

    **Lag 1 - Fysisk lag:**

    - Netværkskabler (f.eks. Ethernet-kabler, fiberoptiske kabler)
    - Hub (fysisk lag, men virker normalt kun på lag 1)

    **Lag 2 - Data link lag:**

    - Switch
    - Access points (Wi-Fi)

    **Lag 3 - Netværkslag:**

    - Router/layer 3 switch
    - IP-telefon

    **Lag 4 - Transportlag:**

    - TCP (Transmission Control Protocol)
    - UDP (User Datagram Protocol)

    **Lag 5 - Sessionlag:**

    - API'er (Application Programming Interfaces) til kommunikation mellem applikationer

    **Lag 6 - Præsentationslag:**

    - JPEG (Joint Photographic Experts Group)
    - MPEG (Moving Picture Experts Group)

    **Lag 7 - Applikationslag:**

    - Webbrowser (f.eks. Google Chrome, Mozilla Firefox)
    - E-mailklient (f.eks. Microsoft Outlook, Gmail)


3. **Find nye/sjældne/esoteriske enheder (fysiske eller virtuelle) (med referencer) - f.eks. next-gen firewalls, proxies, application gateways. Hvad-som-helst med et netstik/wifi er ok at have med.**

    **Deep Packet Inspection (DPI) Enheder:**

    - _Beskrivelse:_ DPI-enheder er avancerede netværksenheder, der analyserer indholdet af data, der passerer gennem et netværk. De kan identificere, klassificere og endda blokere eller prioritere trafik baseret på indhold.

        Eksempel på produkt: Cisco Firepower Threat Defense (FTD)

    - _Placering i OSI-modellen:_ DPI-enheder arbejder primært på lag 7 (Applikationslag), da de analyserer indholdet af datapakker for at forstå applikationsprotokoller og handlinger.

    **Content Delivery Network (CDN) Enheder:**

    - _Beskrivelse:_ CDN-enheder distribuerer indhold (f.eks. websider, billeder, videoer) på tværs af flere servere geografisk placeret for at forbedre ydeevnen og pålideligheden af ​​indholdslevering. 
    
        Eksempel på produkt: **Cloudflare**

    - _Placering i OSI-modellen:_ CDN-enheder fungerer primært på lag 7 (Applikationslag), da de optimerer leveringen af applikationsindhold, men de kan også påvirke ydeevnen på lag 3 (Netværkslag) og lag 4 (Transportlag) ved at dirigere trafik til nærmeste server.

    **Unified Threat Management (UTM) Enheder:**

    - _Beskrivelse:_ UTM-enheder kombinerer flere sikkerhedsfunktioner i én enhed, herunder firewall, intrusion detection/prevention, antivirus, webfiltrering osv. \
Eksempel på produkt: Next-gen Firewall fx: Fortinet Fortigate \

    - _Placering i OSI-modellen:_ UTM-enheder fungerer på flere lag af OSI-modellen, primært på lag 3 (Netværkslag) og lag 7 (Applikationslag), da de tilbyder både netværksbaserede sikkerhedsfunktioner og applikationsbaserede sikkerhedsfunktioner.
4. **Lav en oversigt der tydeligt viser hvilke enheder i har fundet og hvilket lag i OSI modellen de hører til, inkluder det i jeres gruppes gitlab dokumentation.**
    * DPI (Deep Packet Inspection) Eksempel på produkt: Cisco Firepower Threat Defense (FTD): Lag 7
    * UTM (Unified Threat Management) Eksempel på produkt: Cloudflare: Lag 3 og 7
    * CDN (Content Delivery Network) Eksempel på produkt: Next-gen Firewall fx: Fortinet Fortigate: Lag 3, 4 og 7