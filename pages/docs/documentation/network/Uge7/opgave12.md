# Øvelse 12 - Protokoller og OSI modellen

1. **I kali åbn wireshark og lyt på eth0 interfacet indtil i har sniffet en god mængde trafik. Hvis der ikke kommer meget trafik kan i åbne browseren på kali og browse et par sider (gerne nogen der bruger http og ftp hvis i kan finde det) så burde der komme en del trafik.**

2. **Tryk på stop knappen i wireshark (den røde firkant)**
3. **Gem trafikken som en <code>.pcapng</code> fil på kali maskinen i documents mappen**
4. **Lav en liste over de protokoller der optræder i trafikken og placer protokollerne i OSI modellen. (her kan statistics menuen hjælpe med at danne et overblik over protokoller)**
    * **TCP** - Lag 4 
    * **DNS** - Lag 7
    * **TLSv1.3** - Lag 4
    * **OCSP (Online Certificate Status Protocol)**- Lag 7 (bruges til at kontrollere gyldigheden af digitale certifikater)
    * **QUIC** ( Quick UDP Internet Connections)- Lag 4 (forbedre overførslen af webindhold gennem internetforbindelser, kombinerer elementer fra TCP og UDP)
    * **HTTP** - Lag 7
    * **UDP** - Lag 4


5. **Find minimum et eksempel, med kildehenvisning, på hvordan en af jeres fundne protokoller kan misbruges af en trusselsaktør, husk at være kritiske med den kilde i anvender til at underbygge jeres påstand.**

    **UDP DDoS:** Et eksempel på et stort UDP-baseret DDoS-angreb var Mirai-botnet angrebet i oktober 2016. Mirai-botnettet var et botnet bestående af hundredtusindvis af inficerede IoT-enheder, såsom overvågningskameraer, digitale videorekordere og routere. Angriberne bag Mirai udnyttede svagheder i disse enheder til at rekruttere dem til botnettet.

