---
hide:
  - footer
---

# Øvelse 50 - Netværksdiagrammer

## Information

Øvelsen er en gruppeøvelse.

Netværksdiagrammer kan tegnes som fysiske og logiske diagrammer.
Fysiske netværksdiagrammer beskriver hvordan netværksenheder er forbundet fysisk, altså hvordan netværkskablerne er placeret mellem netværkets enheder.  
Logiske diagrammer beskriver hvordan netværkstrafik 

Overblik er en essentiel del i forhold til at implementere sikkerhed, hvis ikke vi har overblik er det svært at vurdere hvor sikkerhed skal implementeres i et netværk.  
Netværksdiagrammer er også en essentiel komponent når netværk skal designes, konstrueres og implementeres.

Formålet med denne øvelse er at få et værktøj til at lave overskuelige netværksdiagrammer samt at få øvet hvordan de tegnes.  
Formålet er også at få repeteret fundamentale begreber som IP adresser, VLAN's og forskellen på fysiske og logiske diagrammer.

![diagram skitse](skitse.png)


## Instruktioner

1. Ovenstående diagrammer skal laves pænt - ie. find et passende diagram værktøj, og lav jeres egen version af det. (Jeg bruger [draw.io](https://www.drawio.com/) med cisco symbolerne. Draw.io kan installeres lokalt, men også som udvidelse i visual studio code)
Fysisk diagram:
![fysisk diagram](fysisk_netværksdiagram.png)
Logisk diagram:
![logisk diagram](logisk_netværksdiagram.png)

2. Tilføj noget associeret tekst der beskriver hvad man ser.
    - I det fysiske netværksdiagram ser vi at de forskellige pc'er er tilknyttet et specifikt VLAN 

    - I det logiske diagram ser et overordnet billede af kommunikationen mellem slutbruger og  ISP
    - CPE står for customer premise equipment og er slut brugeren
    - Trunk tillader at flere VLANs bæres over de samme fysiske forbindelse
3. Diagrammerne skal dokumenteres på jeres gitlab pages. Noter evt. spørgsmål eller tvivl som en del af dokumentationen så jeg kan bruge det i den feedback jeg giver jer i næste undervisningsgang.



