https://ucl-pba-its.gitlab.io/24f-its-nwsec/exercises/52_nw_segmentation_practical_vmware/


Øvelse 52 - Praktisk netværkssegmentering i vmware workstation med opnsense¶

Information¶
Øvelsen er individuel.
I denne øvelse skal du arbejde praktisk med segmentering af netværk i opnsense.
 Formålet er at lære hvordan du kan udvide opnsense med et ekstra netværksinterface og tilhørende netværk.

Instruktioner¶
1. Sluk for opnsense VM. Tilføj et ekstra netværksinterface på samme måde som da du konfigurerede maskinen i Øvelse 20 - OPNsense på vmware workstation 
   ![vmnet4](billeder/vmnet4.png)
   
2. Tilslut netværksinterfacet til vmnet3.     
   ![alt text](billeder/vmnet4settings.png)
   på opnsense
   
3. Tænd for opnsense og konfigurer det nye interface som nedenstående:  
Assign nyt interface, kald det management
![alt text](billeder/interface_assignments.png)   
Konfigurer interfacet som følgende 
![alt text](billeder/interface_assignments2.png)    
![alt text](billeder/interface_assignments3.png)    
![alt text](billeder/interface_assignments4.png) 
![alt text](billeder/interface_assignments5.png)

Tænd for DHCP på netværket og tildel følgende DHCP range  
![alt text](billeder/dhcp.png)   
![alt text](billeder/dhcp2.png) 

4. Tilslut en ny VM til vmnet3 og bekræft at maskinen får tildelt en IP i den ip range du har konfigureret på interfacet.
![alt text](billeder/vmnet3.png)            
-->
![alt text](billeder/xubuntu.png)
 
![alt text](billeder/ip_a.png)
 
Links¶
• How to Create a Basic DMZ (Demilitarized Zone) Network in OPNsense
• OPNsense docs - Interfaces

