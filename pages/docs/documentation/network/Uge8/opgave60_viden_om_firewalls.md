
# Øvelse 60 - Viden om firewalls


## Information

Øvelsen er en gruppeøvelse.

Firewalls er afgørende sikkerhedsenheder, der anvendes til at filtrere og kontrollere trafik mellem et internt netværk og det eksterne internet eller mellem forskellige segmenter af et netværk. \
Der findes forskellige typer af firewalls, og de kan filtrere trafik på forskellige måder. I denne øvelse skal i undersøge forskellige typer af firewalls, hvad de er i stand til og hvilke svagheder de har.

Jeg har givet et par links herunder som inspiration, men i bør selv finde yderligere ressourcer som i kan bruge til at begrunde jeres undersøgelser. Husk at være kildekritiske!


## Instruktioner



1. I jeres gruppe skal i undersøge forskellige firewall typer og hvad de kan. De forskellige typer kan være:
    * Pakkefiltreringsfirewall
    * Stateful Inspection Firewall
    * Application Layer Firewall (Proxy Firewalls)
    * Next-Generation Firewall (NGFW)
    * Proxy Server Firewall (f.eks Web Application Firewall)
2. Lav en kort beskrivelse af hvad de forskellige typer kan, hvilket lag i OSI modellen de arbejder på og hvad deres svagheder er (hvordan de kan kompromitteres ved f.eks ip spoofing)

    **1. Packet Filtering Firewall**


    Funktion: Gennemgår pakker enkeltvis baseret på kilde- og destinationsadresse, portnumre og andre overfladiske karakteristika uden at se på pakkeindholdet.


    OSI Lag: Lag 3 (Netværk) og Lag 4 (Transport).


    Svagheder: Kan blive kompromitteret ved IP spoofing, hvor en angriber forfalsker afsenderadressen i pakkerne for at foregive at være en legitim kilde, hvilket kan omgå filtreringsreglerne.


    **2. Stateful Inspection Firewall**


    Funktion: Holder styr på tilstandsoplysninger for aktive forbindelser og træffer beslutninger baseret på forbindelsens kontekst samt regler.


    OSI Lag: Opererer primært på Lag 3 (Netværk) og Lag 4 (Transport), men kan også inspicere Lag 5 (Session) for at opretholde tilstandsoplysninger.


    Svagheder: Selvom de er mere sikre end packet filtering firewalls, kan de stadig være sårbare over for avancerede angreb som session hijacking og visse DDoS-angreb, der overvælder firewallens tilstandstabeller.


    **3. Application Layer Firewall (Proxy Firewall)**


    Funktion: Inspecter og filtrerer trafik på applikationslaget (Lag 7). Kan foretage en dybere inspektion af indholdet, som HTTP, SMTP, FTP trafik, og blokerer specifikke applikationer eller tjenester.


    OSI Lag: Lag 7 (Applikation).


    Svagheder: Mere ressourcekrævende og kan blive kompromitteret gennem applikationslagets sårbarheder, som SQL injection og cross-site scripting (XSS). Desuden kan krypteret trafik, som HTTPS, kræve dekryptering for inspektion, hvilket rejser privatlivs- og performanceproblemer.


    **4. Next-Generation Firewall (NGFW)**


    Funktion: Integrerer funktionerne af de traditionelle firewalls med avancerede funktioner som dyb pakkeinspektion (DPI), intrusion prevention systems (IPS), og ofte applikationsbevidsthed.


    OSI Lag: Lag 2 til Lag 7.


    Svagheder: Mens NGFW'er tilbyder omfattende beskyttelse, kan de stadig være sårbare over for zero-day angreb (angreb der udnytter hidtil ukendte sårbarheder) og avancerede vedvarende trusler (APT'er), især hvis ikke konfigureret korrekt eller ikke opdateret.


    5. **Proxy server firewall:  \
**inspicerer og filtrerer trafik ved at tvinge alle klient anmodninger gennem proxyen. Denne tilgang tillader detaljeret inspektion og filtrering af indhold, før det når serveren eller før svaret når klienten.


    OSI lag: Lag 7 (Applikation)


    svagheder: udfordringer ved proxy server firewalls, inklusiv WAF'er, er den yderligere latens de introducerer, hvilket kan påvirke applikationens performance.

3. Dokumenter jeres undersøgelse og svar i jeres gruppes gitlab projekt, jeg giver feedback næste gang. Hvis vi har tid så tager vi en kort runde på klassen baseret på jeres spørgsmål.