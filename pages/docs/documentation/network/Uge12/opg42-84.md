# Øvelse 42 - Enumering med offensive værktøjer


## Information

Dette er en gruppeøvelse. Lav den fra jeres Kali maskine på proxmox.

Enumerering er en teknik til at afdække for eksempel hvilke undersider en webside har. Det er en teknik der hører under aktiv scanning->wordlist scanning i Mitre Attack frameworket

Populære værktøjer til at udføre wordlist skanninger er: - gobuster - dirbuster - feroxbuster - ffuf

Værktøjerne er ret støjende ved at de laver en masse forespørgsler til en webserver indenfor kort tid. Det kan detekteres ved at observere netværkstrafikken til webserveren. Mitre skriver _"Monitor for suspicious network traffic that could be indicative of scanning, such as large quantities originating from a single source"_

Øvelsen her giver en kort introduktion til wordlist scanning med udgangspunkt i ffuf Syntaksen til at bruge de forskellige værktøjer er meget ens og derfor burde i kunne afprøve dem på egen hånd efter i har set hvordan ffuf virker.

Kali har et par wordlists installeret i `/usr/share/wordlists/`. Se dem med kommandoen `ls -lh /usr/share/wordlists/`. Udover disse vil jeg anbefale i installerer `seclists` som yderligere wordlists i kali. Instruktionerne herunder anvender wordlists fra seclists.

Husk at dokumentere undervejs!


## Instruktioner



1. Installer seclists med `sudo apt install seclists`
2. Kør en ffuf skanning mod VPLE maskinen og DVWA servicen med `ffuf -r -u &lt;VPLE_IP>:1335/FUZZ -w /usr/share/wordlists/seclists/Discovery/Web-Content/raft-small-words.txt`
3.
![Alt text](Billeder/1.png)

4. Snak i jeres gruppe om hvad i ser. Hvilke http status koder får i, hvad betyder de og hvilke er i mest interesserede i? \
Diskuter hvor i jeres system i kan monitorere at der laves wordlist skanninger? opnsense måske eller ??:

    Vi får statuskode - 200, 301,302,307,200 OK: Succesfuld anmodning, data returneres.


    301 Permanent flyttet: Ressource flyttet permanent til en ny URL.


    302 Fundet (tidligere "Midlertidigt flyttet"): Midlertidig omdirigering til en anden URL.


    307 Midlertidig omdirigering: Midlertidig omdirigering til en anden URL, beholder HTTP-metoden.


    401 Uautoriseret: Anmodning kræver brugerautentisering.


    403 Forbudt: Adgang til ressource er forbudt.


    405 Metode ikke tilladt: Anmodet metode er ikke tilladt for ressourcen.


    500 Intern serverfejl: Uventet fejl på serveren, anmodning kunne ikke gennemføres.401,403,405,500

5. Det er muligt at filtrere ffuf's output med specifikke status koder med parametret `-mc` efter fulgt af statuskoderne f.eks. `200, 302`. \
Det er også muligt at filtrere på størrelse med `-fs` efterfulgt af den størrelse svar i ikke vil have med i outputtet.
6. Lav en ny scanning og filtrer outputtet så i kun ser svar med http respons 200.

    

![Alt text](Billeder/2.png)

Kig i netflow trafikken på opnsense samtidig med at i laver skanningen. Kan i se der at den udføres? \
`ffuf -r -u &lt;VPLE_IP>:1335/FUZZ -w /usr/share/wordlists/seclists/Discovery/Web-Content/raft-small-words.txt -mc 200`



![Alt text](Billeder/3.png)


7. Snak i gruppen om de svar i ser nu. giver det et bedre overblik og ville det være bedre at bruge `-fs` parametret?
8. Prøv at lave ffuf skanningen med en anden wordlist fra `seclists/Discovery/Web-Content` mappen. \
Får i andre resultater?



![Alt text](Billeder/4.png)

9. Lav en ny skanning på en af de fundne url's for eksempel: `&lt;VPLE_IP>:1335/docs/FUZZ`, `&lt;VPLE_IP>:1335/external/FUZZ` eller `&lt;VPLE_IP>:1335/config/FUZZ \
`Hvilke resultater får i og kan i genfinde skannnigerne i nogle logs på jeres system?



![Alt text](Billeder/5.png)





![Alt text](Billeder/6.png)



# Øvelse 82 - Graylog og firewall regler


## Information

Dette er en gruppeøvelse. Øvelsen forudsætter at i har løst Øvelse 81

Det er nødvendigt at tillade trafik fra opnsense på `MANAGEMENT` netværket til graylog på `MONITOR` netværket. Dette for at tillade den data (syslog+netflow) der skal sendes fra opnsense til graylog og for at få adgang til webinterfacet på graylog.


## Instruktioner


## Lav et firewall alias med hhv. port 514 (syslog) , port 2250 (netflow) og port 9100 (webinterface) \




1. Lav en firewall regel der tillader trafikken


![Alt text](Billeder/7.png)

![Alt text](Billeder/8.png)


# Øvelse 83 - Graylog installation i docker via Portainer


## Information

Dette er en gruppeøvelse. Øvelsen forudsætter at i har løst Øvelse 82

Formålet med øvelsen er at guide jer igennem installationen af graylog på docker via portainer.

Graylog bruger 3 docker containere:



* MongoDB som er en `NoSQL` database - Her gemmes rådata som `dokumenter`
* OpenSearch som er en `søgemaskine` - OpenSearch er en opensource fork af elasticsearch
* Graylog som er en løsning til centraliseret logning. Graylog giver mulighed for at søge i data og lave egne dashboards. Det er ikke en erstatning for SIEM systemer, det er udelukkende log opsamling!

Inspirationen til opsætningen kommer fra denne video fra Lawrence Systems - videoen kan være en hjælp når i arbejder: \
Graylog: Your Comprehensive Guide to Getting Started Open Source Log Management


## Instruktioner



1. Jeg baserer min installation på denne docker compose-fil fra dette github repo
2. Generer variabler til miljøet ved hjælp af [dette eksempel](https://github.com/Graylog2/docker-compose/blob/main/open-core/.env.example)



![Alt text](Billeder/9.png)




3. Opret en ny stack i portainer og indsæt indholdet af konfigurationen vist herunder
4. Tilføj de genererede `GRAYLOG_PASSWORD_SECRET` og `GRAYLOG_ROOT_PASSWORD_SHA2` som portainer stackvariabler


![Alt text](Billeder/10.png)

5. Tryk på `Deploy stack`
6. Få adgang til graylogs webgrænseflade via `&lt;docker vm ip>:9100`



![Alt text](Billeder/11.png)


# Øvelse 84 - Graylog konfiguration


## Information

Dette er en gruppeøvelse. Øvelsen forudsætter at i har løst Øvelse 83

I har nu en graylog installation der virker men som endnu ikke er konfigureret. Denne øvelse guider jer gennem konfigurationen af graylog med henblik på at oprette en bruger, gemme data korrekt i databasen og parse data fra opnsense så det bliver søgbart.

Husk at dokumentere undervejs!


## Instruktioner

Instruktionerne vise hvordan graylog konfigureres til at modtage firewall data fra opnsense men ikke netflow data. Når i har gennemført nedenstående skal derfor gentage trin 2, 3 og 4 for netflow data.


### 1. Ny bruger

Root brugerens tidszone er sat til UTC. Det samme er de logs der gemmes. Det er også det helt rigtige, alle logs bør gemmes med UTC tid. Hvis de skal ses i forhold til lokal tid skal det ske ved en omregning mellem UTC og den lokale brugers tidszone. \
Grunden er at det ofte er nødvendigt at kunne sammenstille informationer fra flere forskellige logs og hvis der er gemt som UTC tid vil det være muligt at gøre.

Jeg foretrækker dog at se logsene i min lokale tidszone, og for at gøre det opretter jeg en ny bruger.

**TIP** for at se de konfigurerede tidszoner skal du klikke på din bruger og vælge `System->Overview`, derefter scroll ned for at se de konfigurerede tidszoner for:

User admin: 2023-09-30 13:42:53 +02:00

Your web browser: 2023-09-30 13:42:53 +02:00

Graylog server: 2023-09-30 11:42:53 +00:00



1. Gå til `System->Users and Teams`
2. Opret en bruger med adminprivilegier, skift tidszone til din tidszone
3. Log ud og derefter ind med dine nye brugeroplysninger, bekræft tidszonen i `System->Overview`



![Alt text](Billeder/12.png)



### 2. Konfigurér input

For at få data til graylog skal du oprette inputs, opnsense er konfigureret til at sende syslog data på UDP-port 514 og netflow data på UDP-port 2055.



1. Vælg `System->Inputs`
2. Klik på vælg input og vælg din `source type`, i dette eksempel bruger jeg `SYSLOG UDP`
3. Giv dit input et navn.
4. Vælg den port du modtager data fra. Dette er den interne port for docker-containeren, jeg har for eksempel mappet UDP port 514 udefra containeren til UDP port 5140 inde i containeren (se stack konfigurationen i øvelse 83 for detaljer)
5. Tryk gem
6. Kontrollér, at data modtages ved at klikke på inputtets `Show received messages` knap og bekræft at data modtages




![Alt text](Billeder/13.png)


### 3. Opret indeks[¶](https://ucl-pba-its.gitlab.io/24f-its-nwsec/exercises/84_graylog_config/#3-opret-indeks)

For at dine data gemmes seperat i databasen skal du oprette et indeks.



1. Vælg `System->Indices`
2. Klik på `Create Index Set`
3. Udfyld `Title` `Description` `Index Prefix` (undgå mellemrum i disse)
4. Konfigurér `Index Rotation Configuration` og `Index Retention Configuration` efter jeres præferencer, husk at logs kan blive store og fylde meget på harddisken!
5. Bekræft med `Create Index Set` nederst på siden




![Alt text](Billeder/14.png)

### 4. Opret stream

At placere data i specifikke `streams` gør det nemmere at navigere i data senere.



1. Klik på `Streams` og `Create Streams`
2. Giv jeres stream et navn og eventuelt en beskrivelse
3. Vælg indeks som data skal hentes fra (dit indeks fra tidligere bør vises i dropdown-menuen)
4. Kontrollér at `Remove matches from 'Default Stream'` er valgt så data ikke vises i Default Stream.
5. Bekræft med `Create Stream`





![Alt text](Billeder/15.png)

Statussen for din `Stream` er `paused`, og før du kan bruge den, skal du filtrere, hvilke data der modtages i strømmen, for at gøre dette skal du bruge inputtets `Field:Value` par.



1. Gå til `System->Inputs` og klik på `Show Received Messages`, i det næste vindue kopier strengen ved siden af forstørrelsesglasset, f.eks.: `gl2_source_input:6517dd3c0b3aa72ee5489355`
2. Gå tilbage til `streams` og klik på `More->Manage Rules` på jeres stream.
3. Klik på `Add Stream Rule` for at oprette en ny regel
4. I feltet input skal du indsætte field delen af `Field:Value` parret, f.eks. `gl2_source_input`
5. I værdi-inputtet skal du indsætte value delen af `Field:Value` parret, f.eks. `6517dd3c0b3aa72ee5489355`
6. Test reglen ved at vælge inputtet og `Load Message`, bekræft, at filteret matcher beskeden uden fejl
7. Klik på stream navnet og bekræft, at der indsamles data i den nye stream




![Alt text](Billeder/16.png)



![Alt text](Billeder/17.png)


### 5. Opnsense log konfiguration

Opnsense skal konfigureres til at sende logs til graylog.



1. Syslog data destinationer defineres i `System->Settings->Logging/targets`. Naviger dertil og klik på `+` tegnet for at oprette en ny log destination.
2. Konfigurer destinationen som følgende:



3. Netflow data destination defineres i `Reporting->Netflow`, tilføj graylog som destination og kontroller at i har følgende:



![Alt text](Billeder/18.png)






### 6. Opnsense extractors

Når du modtager data (udover netflow data) fra opnsense i graylog, f.eks. fra firewallen, er det i et format som graylog ikke kender. Derfor skal data parses ved hjælp af en `extractor`. Det kan være en tidskrævende opgave at skrive en `extractor` og heldigvis er der nogen der har gjort det for jer og lagt det i et github repo: \
https://github.com/IRQ10/Graylog-OPNsense_Extractors

Grunden til at det er fedt at data parses, er at det senere er meget lettere at filtrere hvilken data i vil se i f.eks et dashboard. Det kan være baseret på `source ip` eller `action: block`. Hvis ikke dataen er parset så er det ikke muligt at filtrere data.

Inden i konfigurerer extractoren, kan i prøve at filtrere i dataen i jeres `stream`. \
Her vil i nok opdage at data er i en lang tekststreng, fremfor i pæne definerede felter.



1. Åbn `JSON filen` som indeholder `Extractoren` fra https://github.com/IRQ10/Graylog-OPNsense_Extractors/blob/master/Graylog-OPNsense_Extractors_RFC5424.json (ja, der er en masse dejlig regex i den fil....)
2. Åbn `System->Inputs`, klik på `Inputs`
3. For det input, OPNsense videresender til, klik på `Manage extractors`
4. Klik på `Actions` i øverste højre hjørne, og klik derefter på `Import extractors`
5. Indsæt indholdet af `JSON filen` i boksen. Klik på `Add extractors to input`
6. Hvis du bruger RFC 5424-logs: Gå tilbage til inputtet, klik på `More actions` -> `Edit input`, og sørg for, at `Store full message?` er aktiveret.



![Alt text](Billeder/19.png)




### 7. Øvelse 42 igen



1. gentag øvelse 42 og genfind skanningerne i graylog
2. Dokumenter det i gør og det i finder på gitlab

    -port:2055




![Alt text](Billeder/20.png)





![Alt text](Billeder/21.png)




![Alt text](Billeder/22.png)



![Alt text](Billeder/23.png)



![Alt text](Billeder/24.png)
