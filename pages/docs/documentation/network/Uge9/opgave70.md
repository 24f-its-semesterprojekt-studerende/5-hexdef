# Øvelse 70 - Dokumentation og IPAM[¶](https://ucl-pba-its.gitlab.io/24f-its-nwsec/exercises/70_ipam/#velse-70-dokumentation-og-ipam)


## Information[¶](https://ucl-pba-its.gitlab.io/24f-its-nwsec/exercises/70_ipam/#information)

Dette er en gruppeøvelse

Kortlægning af virksomhedens it-systemer, tjenester, enheder osv. er ofte der, hvor sikkerhedsarbejdet starter. Det er fordi det er nødvendigt at vide, hvad vi har for senere at kunne afgøre om og hvordan vi kan beskytte det.

Virksomheder der gerne vil d-mærke certificeres bruger digital trygheds skabelon til at kortlægge hvad de har. \
Skabelonen giver et overblik over virksomheden, men er ikke en detaljeret oversigt over, hvordan netværket er konfigureret.

Inden du fortsætter så hent skabelonen og se hvad den kan i forhold til at kortlægge netværksudstyr? Skabelonen kan finde her: [Template til generel kortlægning]

Netværk kan ofte blive store og derfor er det nødvendigt at holde styr på hvordan det er indrettet med IP adresser. Små netværk kan udmærket dokumenteres med netværksdiagrammer, men i større løsninger er det nødvendigt at have en IPAM (IP Address Management) løsning. IPAM står for "IP Address Management," hvilket på dansk betyder "styring af IP-adresser." \
IPAM refererer til praksissen med at planlægge, administrere og overvåge IP-adresser i et netværk.

Link til [Netbox VM (.ova fil)](https://drive.google.com/file/d/1ZfPr_P2llgnJZsvLj50ZHaIAsgH8b1AA/view?usp=sharing) (er også på itslearning)


## Instruktioner[¶](https://ucl-pba-its.gitlab.io/24f-its-nwsec/exercises/70_ipam/#instruktioner)



1. Læs om IPAM og snak i jeres gruppe om hvad det er og hvad det bruges til, brug links herunder til at undersøge IPAM:
    * [Bluecat - what-is-ipam](https://bluecatnetworks.com/glossary/what-is-ipam/)
    * [OpUtils - What is IPAM?](https://www.manageengine.com/products/oputils/what-is-ipam.html)
2. papir+blyant øvelse. \
Lav en IP subnet oversigt over devices i har derhjemme og de tilhørende interne og eksterne netværk.
3. Research hvad netbox er, lav en liste over hvad det kan og hvad det ikke kan.
* Dokumentering af netværk
* Modellering af netværk
* kombinere IPAM og DCIM (database infrastucture management)
* Giver en source of truth

        Det den ikke kan: \
tegne netværket. Der skal man bruge et eksternt redskab

* kan ikke scanne efter ip adresser (_https://github.com/netbox-community/netbox/wiki/Frequently-Asked-Questions)_
* 
4. Set netbox VM op i VMware workstation og log ind på web interfacet. Brug [getting started](https://docs.netbox.dev/en/stable/getting-started/planning/) fra netbox dokumentationen (som er lavet med mkdocs!!) \

5. Dokumentér et valgfrit netværk via netbox web interfacet. Søg på nettet efter _large network diagram_ eller _large network topology_ f.eks. [CISCO-example.png](https://www.conceptdraw.com/How-To-Guide/picture/CISCO-example.png) \



![alt text](billeder/Billede13.png)


    


![alt text](billeder/Billede14.png)



    Netbox credentials:  


    consolen viser ip adressen


    netbox:netbox som password til web interface.


    andre logins i console

6. Diskuter i gruppen hvad i synes er en god måde at holde styr på et større netværk med perspektiv til hvordan i kan gøre det i eksamensprojektet? \
Research evt. hvilke andre måder i kan lave IPAM dokumentation på i eksamensprojektet.

At holde styr på et større netværk kræver en struktureret tilgang og brug af passende værktøjer. Her er nogle overvejelser og muligheder for at håndtere et større netværk, med fokus på eksamensprojektet:



1. **Netværksmonitorering**: Brug af netværksmonitoreringsværktøjer som Nagios, Zabbix eller PRTG til at overvåge netværksaktivitet, ydeevne og tilgængelighed af enheder og tjenester. Dette giver mulighed for proaktiv fejlfinding og vedligeholdelse.
2. **Netværksdokumentation**: Brug af værktøjer som NetBox, Device42 eller SolarWinds IPAM til at dokumentere netværkskomponenter, konfigurationer og forbindelser. Disse værktøjer hjælper med at holde styr på IP-adresser, VLAN'er, enheder og deres placering i netværket.
3. **Konfigurationsstyring**: Implementering af konfigurationsstyringsværktøjer som Ansible, Puppet eller Chef til at automatisere konfigurationsændringer og sikre konsekvens i netværksenheder.
4. **Sikkerhed og adgangsstyring**: Implementering af sikkerhedsforanstaltninger som firewall-regler, VPN-adgang og netværkssegmentering for at sikre netværkets integritet og beskyttelse mod trusler.
5. **Skalering og redundans**: Overvej implementering af redundante netværksforbindelser, load balancing og failover-mekanismer for at sikre høj tilgængelighed og skalerbarhed.

I forhold til eksamensprojektet kan du undersøge og overveje følgende alternative metoder til IPAM-dokumentation:



1. **Manuel IPAM-dokumentation**: Brug af regneark eller tekstfiler til at dokumentere IP-adresser, VLAN'er og tilknyttede enheder. Dette er en simpel tilgang, men kan være besværlig at opretholde og mangler ofte integration med andre netværksværktøjer.
2. **IPAM-værktøjer i skyen**: Brug af IPAM-værktøjer, der er baseret på cloud, såsom AWS IP Address Management eller Azure IPAM-tjenester, hvis eksamensprojektet involverer cloud-baseret infrastruktur.
3. **Hjemmelavede løsninger**: Udvikling af en brugerdefineret IPAM-løsning ved hjælp af programmeringssprog som Python eller Ruby og databaseløsninger som MySQL eller PostgreSQL. Dette giver mulighed for tilpasning, men kræver mere tid og ekspertise at implementere.

Uanset hvilken metode der vælges, er det vigtigt at sikre, at IPAM-dokumentationen er nøjagtig, opdateret og tilgængelig for de relevante interessenter i eksamensprojektet.

NetBox is an open-source web application designed for tracking and managing infrastructure, including networks. If you have a picture of a network and you want to document it in NetBox, you'll need to create the corresponding objects (devices, interfaces, connections, etc.) within NetBox based on the information in the picture. Here's a step-by-step guide to get you started:



1. **Install NetBox**: First, you need to have NetBox installed and running. You can follow the official installation guide here: NetBox Installation Guide.
2. **Access NetBox**: Once NetBox is installed, access the web interface using your preferred web browser.
3. **Log in**: Log in to NetBox using your credentials.
4. **Navigate to Device List**: Once logged in, navigate to the Device List page. This is where you'll create and manage devices.
5. **Add Devices**: Based on the picture of your network, start adding devices (routers, switches, firewalls, etc.) to NetBox by clicking on the "+ Add" button or similar.
6. **Fill in Device Details**: For each device, fill in details such as name, site, device type, manufacturer, etc. These details should correspond to the devices in your network picture.
7. **Add Interfaces**: For each device, add interfaces (Ethernet ports, serial ports, etc.) as needed. Specify details like name, type, MAC address, etc.
8. **Connect Devices**: If your network picture shows how devices are connected, create connections between devices in NetBox. You can do this by creating cables and specifying the interfaces they connect to on each device.
9. **Document Additional Information**: Document any additional information about each device, such as IP addresses, VLANs, configurations, etc., either directly in NetBox or by linking to external documentation.
10. **Review and Validate**: Once you've added all the devices and connections, review your entries to ensure accuracy and completeness.
11. **Save Changes**: Save your changes in NetBox.
12. **Document the Network Diagram**: While NetBox itself doesn't provide a graphical network diagramming tool, you can use external tools like draw.io, Lucidchart, or Microsoft Visio to create a visual representation of your network based on the information stored in NetBox.