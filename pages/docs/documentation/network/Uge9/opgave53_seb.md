Øvelse 53 - Netværksdesign¶
Dette er en individuel øvelse.

Formålet med øvelsen er at få rutine i at designe sikre segmenterede netværk. Det vil være nødvendigt at kunne som en del af eksamensprojektet og det er også en god øvelse i at få rutine med grundlæggende netværk.

Netværket der skal designes skal understøtte følgende enheder:

1 OPNsense router (du bestemmer selv antal interfaces)
1 webserver vm der server en intranet side via https
1 server vm der opsamler netværkslog fra OPNsense (her benyttes graylog i docker compose)
1 server vm der kører usikre services i vulnerable-pentesting-lab-environment
1 kali vm der benyttes af netværksadministratoren til at teste netværket
Instruktioner¶
Lav en inventarliste med minimum porte og protokoller som services på de virtuelle maskiner benytter. Du må gerne tilføje mere hvis du mener det er nødvendigt.

**Lav et netværksdiagram med passende segmentering til de virtuelle maskiner. Lav segmentering med seperate interfaces der hver har 1 netværk. Anvend ikke VLANs.**
![Alt text](billeder/etdaarligteks.png)

**Tegn firewalls på netværksdiagrammet - angiv i en tabel hvilke regler du vil sætte op på de enkelte firewalls**
Firewall: Webserver   - Tillad indgående trafik til port 443 (HTTPS) fra internet.

- Tillad kun udgående trafik, der er relevant for webserverens drift.

Firewall: Usikre Services    - Tillad indgående trafik til port 80 (HTTP) og port 22 (SSH) fra LAN.

- Bloker indgående trafik fra internettet til usikre services.
Tillad intern trafik mellem interne enheder/Servere

**Redegør for dine valg omkring segmentering og firewall regler. Det vil sige at andre skal kunne forstå hvad du har valgt at gøre og hvorfor du har valgt at gøre det.**
fysisk segmentering uden brug af VLAN giver klarhed og enkelhed i netværksdesignet
Hver enhed har sin egen fysiske interface, det giver mere kontrol og segmentering da enheder ikke deler samme netværksforbindelse

ved at sikre, at kun relevant trafik tillades, kan man også forhindre overbelastning af netværksressource