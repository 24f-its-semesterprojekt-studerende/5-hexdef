https://ucl-pba-its.gitlab.io/24f-its-nwsec/exercises/53_network_design/


Øvelse 53 - Netværksdesign¶

Information
Dette er en individuel øvelse.      
Formålet med øvelsen er at få rutine i at designe  sikre segmenterede netværk.  Det vil være nødvendigt at kunne som en del af eksamensprojektet og det  er også en god øvelse i at få rutine med grundlæggende netværk.   
Netværket der skal designes skal understøtte følgende enheder:  
• 1 OPNsense router (du bestemmer selv antal interfaces)  
• 1 webserver vm der server en intranet side via https  
• 1 server vm der opsamler netværkslog fra OPNsense (her benyttes graylog i docker compose)  
• 1 server vm der kører usikre services i vulnerable-pentesting-lab-environment  
• 1 kali vm der benyttes af netværksadministratoren til at teste netværket  


Instruktioner  
1. Lav en inventar liste med minimum porte og protokoller som services på de virtuelle maskiner benytter. Du må gerne tilføje mere hvis du mener det er nødvendigt. 
   
![alt text](billeder/emil/grid1.png)
![alt text](billeder/emil/141-1.png)   
![alt text](billeder/emil/141-2.png)  

2. Lav et netværksdiagram med passende segmentering til de virtuelle maskiner. Lav segmentering med seperate interfaces der hver har 1 netværk. Anvend ikke VLANs.
   idk jeg fatter ikke noget:
![alt text](billeder/emil/141-3.png)   
   
3. Tegn firewalls på netværksdiagrammet - angiv i en tabel hvilke regler du vil sætte op på de enkelte firewalls
   
![alt text](billeder/emil/grid2.png)

4. Redegør for dine valg omkring segmentering og firewall regler. Det vil sige at andre skal kunne forstå hvad du har valgt at gøre og hvorfor du har valgt at gøre det.  
   Segmentering: Bare segmenter alting. Især VPLE serveren er vigtig at holde adskilt fra de andre, da den især er sårbar.  
   Man kunne vel eventuelt godt have log serveren i samme segment, som Kali admin pc'en.
   
   Opnsense:   
   Intranet:  
   Log:  
   Kali: Den skal vel bare have nogle ret standard regler. Tillad al trafik fra bag firewallen og bloker det ude fra?  
   VPLE: Siden den indeholder en masse usikre services er der ikke smart, hvis trafikker kan komme fra denne VM og videre ud i netværket 
   
5. Sammenlign med dine gruppemedlemmer. Er der forskelle? 
Jeg kigger på jeres gitlab dokumentation og giver feedback næste undervisningsgang.
