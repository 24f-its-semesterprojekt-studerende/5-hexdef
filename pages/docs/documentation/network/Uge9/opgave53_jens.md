## Øvelse 53 - Netværksdesign

Dette er en individuel øvelse.

Formålet med øvelsen er at få rutine i at **designe** sikre segmenterede netværk. Det vil være nødvendigt at kunne som en del af eksamensprojektet og det er også en god øvelse i at få rutine med grundlæggende netværk.

Netværket der skal **designes** skal understøtte følgende enheder:



* 1 OPNsense router (du bestemmer selv antal interfaces)
* 1 webserver vm der server en intranet side via https
* 1 server vm der opsamler netværkslog fra OPNsense (her benyttes [graylog i docker compose](https://github.com/Graylog2/docker-compose/blob/main/open-core/docker-compose.yml))
* 1 server vm der kører usikre services i [vulnerable-pentesting-lab-environment](https://www.vulnhub.com/entry/vulnerable-pentesting-lab-environment-1,737/)
* 1 kali vm der benyttes af netværksadministratoren til at teste netværket


## Instruktioner



1. **Lav en inventarliste med minimum porte og protokoller som services på de virtuelle maskiner benytter. Du må gerne tilføje mere hvis du mener det er nødvendigt.**

**OPNsense Router: \
Porte:** \
LAN-interface: Port 1 (Ethernet), Port 2 (Ethernet) \
WAN-interface: Port 3 (Ethernet) \
**Protokoller:** \
HTTP (port 80), HTTPS (port 443) for administration \
 \
**Webserver VM: \
Porte:** \
HTTP: Port 80, HTTPS: Port 443 \
**Protokoller:** \
HTTP, HTTPS \
 \
**Graylog Server VM:** \
**Porte:** \
UDP: Port 514 (Syslog) \
TCP: Port 9000 (Web Interface), Port 514 (Syslog), Port 12201 (GELF) \
**Protokoller:** \
Syslog (UDP/TCP), GELF \
 \
**Vulnerable Server VM:** \
**Porte:** \
Porte afhænger af de usikre tjenester, der køres på serveren. (f.eks. Port 22 for SSH, Port 21 for FTP, osv.) \
**Protokoller:** \
Afhænger af de usikre tjenester \
 \
**Kali VM:** \
**Porte:** \
Porte afhænger af de værktøjer og services, der bruges til testformål. \
**Protokoller:** \
Afhænger af de værktøjer og tjenester, der bruges til testformål.


    



2. **Lav et netværksdiagram med passende segmentering til de virtuelle maskiner. Lav segmentering med seperate interfaces der hver har 1 netværk. Anvend ikke VLANs.**

![alt text](billeder/opgave53_jens.png)

3. **Tegn firewalls på netværksdiagrammet - angiv i en tabel hvilke regler du vil sætte op på de enkelte firewalls**

### **Firewall Regler:**

**OPNsense Firewall:**

Tillad trafik fra WAN til Webserver VM på port 443 (HTTPS)

Tillad trafik fra WAN til Graylog Server VM på port 514 (Syslog)

Tillad kun specifik trafik fra WAN til Vulnerable Server VM (f.eks. begræns adgang til kun nødvendige tjenester som SSH eller andre tjenester) \
 \
**Webserver VM:**

Tillad indgående trafik fra WAN på port 443 (HTTPS)

 \
**Graylog Server VM:**

Tillad indgående trafik fra WAN på port 514 (Syslog)

Tillad indgående trafik fra LAN på port 9000 (Web Interface)

Tillad indgående trafik fra LAN på port 514 (Syslog)

Tillad indgående trafik fra LAN på port 12201 (GELF)

 \
**Vulnerable Server VM:**

Specifikke regler afhænger af de usikre tjenester, der kører på serveren. For eksempel, hvis der kører en usikker webapplikation, skal der være regler for at tillade trafik til den relevante port.

Dette diagram er baseret på en simpel netværkskonfiguration og firewall-regler. Afhængigt af dine specifikke behov og sikkerhedskrav kan det være nødvendigt at justere eller tilføje flere regler.



4. **Redegør for dine valg omkring segmentering og firewall regler. Det vil sige at andre skal kunne forstå hvad du har valgt at gøre og hvorfor du har valgt at gøre det.**
5. **Sammenlign med dine gruppemedlemmer. Er der forskelle?**