# Opgave 12 - Linux log system.


## Information

I Linux har to log systemer. rsyslog som er en nyere udgave af syslog, og journalctl. \
I de kommende opgaver kigger vi på rsyslog, og log filerne i ubuntu (debian).

Log filerne opbevares i `/var/log/`.


## Instruktioner


### primærer log file

Den primær log file for systemet hedder `syslog`, og indeholder information om \
stort alt systemet foretager sig.



1. udskriv inholdet af denne file.
2. studer log formattet, og skriv et "generisk" format ind i dit Linux cheat sheet.
3. Noter hvilken tids zone loggen bruger til tidsstemple, er det UTC?

    Kommando: cat /var/log/syslog


    Timezone: CET


    Billede:![alt text](Billeder/syslog.png)



### Authentication log

Log filen `auth.log` indeholder information om autentificering.



1. udskriv indholdet af `auth.log`
2. skrift bruger til F.eks. root
3. udskriv indholdet af `auth.log` igen, og bemærk de nye rækker
4. skrift tilbage til din primær bruger igen (som naturligvis ikke er root)
5. udskriv indholdet af `auth.log` igen, og bemærk de nye rækker

    Kommando: cat /var/log/auth.log





    Billede: 
    ![alt text](Billeder/auth.log.png)
