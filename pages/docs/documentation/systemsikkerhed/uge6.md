### Operativsystem Sikkerhed


#### Opgave 1. Læringsmål

Læs og forstå læringsmålene
Hvert teammedlem skal individuelt læse og reflektere over studieordningens læringsmål for faget System sikkerhed.

Konkrete eksempler til hvert læringsmål
	
	Viden: 
Generelle governance principper / sikkerhedsprocedurer - CIS 18 kontrol til benchmarking, standarder(ISO 27000-serie, NIST)

Væsentlige forensic processer - Sentinel, 2FA

Relevante it-trusler - Phishing/ransomware, OWASP top 10

Relevante sikkerhedsprincipper til systemsikkerhed - CIS18, least privilege

OS roller ift. sikkerhedsovervejelser - Least privilege 

Sikkerhedsadministration i DBMS - Forkert bruger får rettigheder til at ændre? Udnytte modforanstaltninger til sikring af systemer, least privilege


Færdigheder: 
Udnytte modforanstaltninger til sikring af systemer - IPS, IDS

Følge et benchmark til at sikre opsætning af enhederne - CIS 18 kontroller

Implementere systematisk logning og monitering af enheder - SIEM, SNORT(IDS & IPS)

Analysere logs for incidents og følge et revisionsspor - SIEM, SPLUNK, Logpoint

Kan genoprette systemer efter en hændelse - BACKUP






Kompetencer: 
Håndtere enheder på command line-niveau - Linux

Håndtere værktøjer til at identificere og fjerne/afbøde forskellige typer af endpoint trusler - Firewall

Håndtere udvælgelse, anvendelse og implementering af praktiske mekanismer til at forhindre, detektere og reagere over for specifikke it-sikkerhedsmæssige hændelser - IDS, IPS

Håndtere relevante krypteringstiltag - AES, TLS, hash.




#### Opgave 2. Opsætning af VM (Ubuntu)

https://ubuntu.com/download/server
