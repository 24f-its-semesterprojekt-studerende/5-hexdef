# Opgave 19 - Installation af iptables


## Information

**I Linux håndteres netværksfiltreringen (firewall) med et kerne-modul, der hedder <code>Netfilter</code>. At interagere direkte med <code>Netfilter</code>-modulet er komplekst og ineffektivt. Derfor anvendes der typisk et interface til at interagere med <code>Netfilter</code>. Et eksempel på et sådant interface er iptables. I undervisningen bruger vi iptables til implementering af firewall-regler.</strong>

**I den kommende opgave skal iptables installeres.**

**I alle øvelserne kan du evt. bruge NMap til at test firewall konfigurationen, eller ved at pinge hosten fra en anden host**


## Instruktioner



1. **Installer iptables med kommandoen <code>sudo apt install iptables</code></strong>
2. <strong>Udskriv versionsnummeret med kommandoen <code>sudo iptables -V</code></strong>
3. <strong>Installer iptables-persistent med kommandoen <code>sudo apt install iptables-persistent</code></strong>



![alt text](Billeder/Billede1.png)




4. **Udskriv alle firewall-regler (regelkæden) med kommandoen <code>sudo iptables -L</code></strong>



![alt text](Billeder/Billede2.png)




5. **Reflekter over, hvad der menes med firewall-regler, samt hvad der menes med "regelkæden".**

**Når man har ændret en iptables firewall-regel, skal man manuelt gemme den nuværende regelopsætning med kommandoen <code>sudo netfilter-persistent save</code>, ellers er regellisten tom efter genstart.</strong>


# Opgave 20 - Bloker alt indgående trafik


## Instruktioner[¶](https://24f-its-syssec-ucl-pba-its-3db254c7a2dc5a246e5621c25301b2b971d3.gitlab.io/exercises/20_Linux_Dissallowing_everything_iptables/#instruktioner)



1. **Udskriv regelkæden og notér dig, hvordan den ser ud. (Regelkæden er tidligere blevet udskrevet i [opgave 19](https://24f-its-syssec-ucl-pba-its-3db254c7a2dc5a246e5621c25301b2b971d3.gitlab.io/exercises/19_Linux_Installing_iptables/), trin 4)**
2. **Lav en regel i slutningen af regelkæden, som dropper alt trafik, med kommandoen <code>sudo iptables -A INPUT -j DROP</code>.</strong>
3. <strong>Udskriv regelkæden igen og notér forskellen fra trin 1.</strong>



![alt text](Billeder/Billede3.png)



# Opgave 21 - Tillad indgående trafik fra etableret forbindelser


## Instruktioner[¶](https://24f-its-syssec-ucl-pba-its-3db254c7a2dc5a246e5621c25301b2b971d3.gitlab.io/exercises/21_Linux_Allow_Self_established_connections/#instruktioner)



1. **Eksekver kommandoen <code>sudo iptables -I INPUT -m conntrack --ctstate ESTABLISHED,RELATED -j ACCEPT</code></strong>
2. <strong>Udskriv reglen listen.</strong>

<strong>I kommandoen vælger vi at indsætte reglen først i regelen kæden med <code>-I</code> muligheden. Herefter vælges det er et iptables module med <code>-m conntrack</code> (conntrack==connection track) for at iptables kan holde styr på hvilken forbindelser der er etableret, og af hvem. Til sidst bruges muligheden <code>--ctstate ESTABLISHED,RELATED -j ACCEPT</code> som tillader indadgående pakker fra serverer hvor klienten(OS) selv har oprettet forbindelse.</strong>




![alt text](Billeder/Billede4.png)


# Opgave 23 - Tillad indgående trafik fra specifikke ICMP-beskeder.


## Instruktioner[¶](https://24f-its-syssec-ucl-pba-its-3db254c7a2dc5a246e5621c25301b2b971d3.gitlab.io/exercises/23_Linux_Allowing_specific_ICMP_messages/#instruktioner)



1. **Tillad ICMP-pakker af type 3 med kommandoen <code>sudo iptables -A INPUT -m conntrack -p icmp --icmp-type 3 --ctstate NEW,ESTABLISHED,RELATED -j ACCEPT</code>.</strong>
2. <strong>Tillad ICMP-pakker af type 11 med kommandoen <code>sudo iptables -A INPUT -m conntrack -p icmp --icmp-type 11 --ctstate NEW,ESTABLISHED,RELATED -j ACCEPT</code>.</strong>
3. <strong>Tillad ICMP-pakker af type 12 med kommandoen <code>sudo iptables -A INPUT -m conntrack -p icmp --icmp-type 12 --ctstate NEW,ESTABLISHED,RELATED -j ACCEPT</code>.</strong>
4. <strong>Udskriv regelkæden. Kan du finde fejlen? og hvordan kan den rettes?</strong>
5. <strong>De ICMP-pakker, der er blevet tilladt, er hver især af en bestemt type. Undersøg hvad hver af de 3 typer betyder.</strong>



![alt text](Billeder/Billede5.png)
