# **Linux Cheatsheet**


### Opgave 3 - Navigation i Linux filesystem med bash


**Eksekver kommandoen pwd:**


Viser den aktuelle arbejdsmappe (aktuelt arbejdssted).


**Eksekver kommandoen cd ..:**


Skifter til den overordnede mappe (en mappe op).


**Eksekver kommandoen cd /:**


Skifter til rodmappen.


**Lokationen “/” har en bestemt betydning i Linux-filsystemet. Hvad er det?**


/ repræsenterer rodmappen i Linux-filsystemet, det øverste niveau, hvor alle andre mapper og filer findes.


**Eksekver kommandoen cd /etc/ca-certificates/. Hvad findes der i direktoriet?**


Dette vil skifte til mappen /etc/ca-certificates/, som sandsynligvis indeholder SSL/TLS-certifikater og tilhørende konfigurationsfiler.


**Hvor mange directories viser outputtet fra pwd kommandoen når den eksekveres i direktoriet fra forrige trin?**


Outputtet fra pwd viser kun én mappe, som er /etc/ca-certificates/.


**Eksekver kommandoen cd ../..:**


Skifter to mapper op fra den aktuelle placering.


**Hvor mange directories viser outputtet fra pwd kommandoen nu?**


Outputtet fra pwd vil vise en mappe, to niveauer over den oprindelige.


**Eksekver kommandoen cd ~:**


Skifter til brugerens home directory.


**Kommandoen ~ er en "genvej" i Linux, hvad er det en genvej til?**


~ er en genvej til brugerens home directory.


**I filsystemets rod (/), eksekver kommandoen ls:**


Viser alle filer og mapper i rodmappen.


**I brugerens home directory, eksekver kommandoen touch helloworld.txt:**


Opretter en tom fil med navnet helloworld.txt.


**I brugerens home directory, eksekver kommandoen nano helloworld.txt:**


Åbner filen helloworld.txt i teksteditoren nano.


**List alle filer og mapper i brugerens home directory:**


ls


**List alle filer og mapper i brugerens home directory med flaget -a:**


ls -a


**List alle filer og mapper i brugerens home directory med flaget -l:**


ls -l


**List alle filer og mapper i brugerens home directory med flaget -la:**


ls -la


**I brugerens home directory, eksekver kommandoen mkdir helloWorld:**


Opretter en ny mappe med navnet helloWorld.


**Eksekver kommandoen** ls -d */:


Viser kun mapper i det aktuelle directory.


**Eksekver kommandoenn ls -f:**


Viser filer og mapper i det aktuelle directory uden at sortere.


### Opgave 4 - Linux file struktur


**Forklaring af typiske datatyper i de forskellige Linux root folders:**


**/bin:** Kort for "binaries", indeholder eksekverbare programmer og kommandoer, som er essentielle for systemets basale funktioner. Eksempler inkluderer shell-kommandoer som ls, cp, mv, osv.


**/boot:** Indeholder filer, der er nødvendige under opstarten af systemet, såsom kernelbilleder, bootloaderkonfigurationsfiler og andre relaterede filer.


**/dev:** Kort for "devices", indeholder filer, der repræsenterer systemets enheder, såsom harddiske, USB-enheder, serielportenheder, osv.


**/etc:** Indeholder konfigurationsfiler for systemet og installerede applikationer. Dette inkluderer netværkskonfiguration, bruger- og gruppedefinitioner, systemtjeneste konfigurationer og meget mere.


**/home:** Indeholder brugerens home directories, hvor brugerne typisk har tilladelse til at gemme personlige filer og data.


**/media:** Bruges til at montere midlertidige filsystemer, såsom USB-drev, CD-ROM'er, eksterne harddiske, osv.


**/lib:** Indeholder biblioteker, der er nødvendige for kørsel af programmer i /bin og /sbin, samt kernelmoduler.


**/mnt:** Traditionelt brugt til midlertidigt at montere eksterne filsystemer, men bruges nu mindre hyppigt til fordel for /media.


**/misc:** Reserveret til diverse filer og mapper, der ikke falder ind under andre kategorier.


**/proc:** En virtuel filsystem, der indeholder information om kørende processer, systemressourcer og andre systemrelaterede oplysninger.


**/opt:** Anvendes til at installere ekstra softwarepakker, typisk ikke-distribueret som en del af det basale operativsystem.


**/sbin:** Korte for "system binaries", indeholder eksekverbare programmer, der primært bruges af systemadministratorer til systemvedligeholdelse og konfiguration.


**/root:** Brugerens home directory for root-brugeren, superbrugeren i Linux-systemet.


**/tmp:** Bruges til midlertidige filer, der slettes automatisk ved genstart.


**/usr:** Indeholder brugerrelaterede programmer, biblioteker, dokumentation og andre ikke-essentielle systemfiler.


**/var:** Indeholder variable datafiler, såsom logfiler, mail, printerfiler, cache og midlertidige databaser.


Opgave 6 - Søgning i Linux file strukturen


**I Home directory, eksekver kommandoen find:**


Dette vil søge rekursivt efter filer og mapper i den aktuelle brugers home directory og alle undermapper.


**I Home directory, eksekver kommandoen find /etc/.:**


Dette vil søge rekursivt efter filer og mapper i /etc/ mappen og alle dens undermapper.


**Eksekver kommandoen sudo find /etc/ -name passwd:**


Dette vil søge efter en fil med navnet "passwd" i /etc/ mappen og dens undermapper. Brugen af sudo betyder, at kommandoen køres med de rettigheder, der er tilladt for brugere i sudo-gruppen.


**Eksekver kommandoen sudo find /etc/ -name pasSwd. Husk stort S:**


Dette vil søge efter en fil med navnet "pasSwd" (med stort 'S') i /etc/ mappen og dens undermapper.


**Eksekver kommandoen sudo find /etc/ -iname pasSwd. Husk stort S:**


Dette vil søge efter en fil med navnet "pasSwd", uanset om det er stort eller småt 'S', i /etc/ mappen og dens undermapper.


**Eksekver kommandoen** sudo find /etc/ -name pass*:


Dette vil søge efter filer, der starter med "pass", i /etc/ mappen og dens undermapper.


**I Home directory, eksekver kommandoen truncate -s 6M filelargerthanfivemegabyte:**


Dette vil oprette en fil ved navn filelargerthanfivemegabyte med en størrelse på 6 megabyte.


**I Home directory, eksekver kommandoen truncate -s 4M filelessthanfivemegabyte:**


Dette vil oprette en fil ved navn filelessthanfivemegabyte med en størrelse på 4 megabyte.


**I roden (/), eksekver kommandoen find /home -size +5M:**


Dette vil søge efter filer i /home directory, som er større end 5 megabyte.


**I roden (/), eksekver kommandoen find /home -size -5M:**


Dette vil søge efter filer i /home directory, som er mindre end 5 megabyte.


**I Home directory, opret to directories, et der hedder test og et andet som hedder test2:**


Dette vil oprette to mapper ved hjælp af kommandoen “mkdir”, test og test2, i den aktuelle brugers home directory.


**I test2, skal der oprettes en fil som hedder test:**


Dette vil oprette en fil, ved hjælp af kommandoen “touch”, med navnet test i mappen test2.


**I Home directory, eksekver kommandoen find -type f -name test:**


Dette vil søge efter filer med navnet test i den aktuelle brugers home directory og dens undermapper.


**I Home directory, eksekver kommandoen find -type d -name test:**


Dette vil søge efter mapper med navnet test i den aktuelle brugers home directory og dens undermapper.


### Opgave 7 - Ændring og søgning i filer


**I Home directory, eksekver kommandoen touch minfile.txt:**


Opretter en tom fil med navnet minfile.txt.


**I Home directory, eksekver kommandoen cp minfile.txt kopiafminfile.txt:**


Kopierer filen minfile.txt og opretter en kopi med navnet kopiafminfile.txt.


**I Home directory, eksekver kommandoen mkdir minfiledir:**


Opretter en ny mappe med navnet minfiledir.


**I Home directory, eksekver kommandoen mv minfile.txt minfiledir:**


Flytter filen minfile.txt til mappen minfiledir.


**I Home directory, eksekver kommandoen rm -r minfiledir:**


Fjerner rekursivt mappen minfiledir og alt indholdet i den.


**I de næste trin skal der arbejdes med oprettelse af filer med tekst indhold, og her bliver redirect-operatoren (>) introduceret. Operatoren tager outputtet fra kommandoen på venstre side og skriver til filen på højre side.**


**I Home directory, eksekver kommandoen echo "hej verden" > hejverdenfil.txt:**


Opretter en ny fil med navnet hejverdenfil.txt og skriver teksten "hej verden" til den.


**I Home directory, eksekver kommandoen cat hejverdenfil.txt:**


Viser indholdet af filen hejverdenfil.txt på skærmen.


**I Home directory, eksekver kommandoen echo "hej ny verden" > hejverdenfil.txt:**


Overskriver indholdet af filen hejverdenfil.txt med teksten "hej ny verden".


**I Home directory, eksekver kommandoen cat hejverdenfil.txt:**


Viser det nye indhold af filen hejverdenfil.txt på skærmen.


**I Home directory, eksekver kommandoen echo "hej endnu en ny verden" >> hejverdenfil.txt:**


Tilføjer teksten "hej endnu en ny verden" til slutningen af filen hejverdenfil.txt uden at overskrive det eksisterende indhold.


**I Home directory, eksekver kommandoen cat hejverdenfil.txt:**


Viser det endelige indhold af filen hejverdenfil.txt på skærmen, inklusive alle tidligere linjer og den nye tilføjede linje.


**I de næste trin introduceres pipe-operatoren (|). Den tager outputtet fra kommandoen på venstre side og giver det videre til kommandoen på højre side.**


**I /etc/, eksekver kommandoen cat adduser.conf:**


Viser indholdet af filen adduser.conf på skærmen.


**I /etc/, eksekver kommandoen cat adduser.conf | grep 1000:**


Søger efter linjer indeholdende "1000" i filen adduser.conf og viser dem på skærmen.


**I /etc/, eksekver kommandoen cat adduser.conf | grep no:**


Søger efter linjer indeholdende "no" i filen adduser.conf og viser dem på skærmen.


**I /, eksekver kommandoen grep no /etc/adduser.conf:**


Søger efter linjer indeholdende "no" i filen adduser.conf i /etc/ directory og viser dem på skærmen.


**I /, eksekver kommandoen ls -al | grep proc:**


Viser resultaterne af ls -al kommandoen (som lister indholdet af rodmappen med detaljerede rettigheder) og filtrerer kun de linjer, der indeholder "proc".


**I /etc/, eksekver kommandoen ls -al | grep shadow:**


Viser resultaterne af ls -al kommandoen (som lister indholdet af /etc/ directory med detaljerede rettigheder) og filtrerer kun de linjer, der indeholder "shadow". Dette vil sandsynligvis søge efter filen shadow, som indeholder sikkerhedsfølsomme oplysninger om brugeradgangskoder.