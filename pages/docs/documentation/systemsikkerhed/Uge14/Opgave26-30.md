# Opgave 26 - Installer auditd


## Information

**Audit daemon (auditd) er Linux's audit system. Auditd kan overvåge jvf. tilladelser, altså Read,Write & Excute. Typisk anvendes audit systemet til overvågning af filer, directories & system kald(Kald til OS API'et).**

**Audit daemon fungere principielt som et logging system for prædefineret begivenheder. Alle begivenheder defineres som <code>Audit regler</code>. Et eksempel på et prædefineret begivenhed kunne være ændringer i en bestemt file. Hver gang der bliver ændret i filen, udløser dette en begivenhed som resulter i et <code>Audit log</code>. En slags "alarm" der udløses af en konkret begivenhed. Som udgangspunkt kan alle <code>auditd</code> logs findes i <code>/var/log/audit/audit.log</code>.</strong>

**Generelt er auditd ikke særligt intuitivt at arbejde med, men det skaber en god forståelse for hvordan et sådan system virker.**

**Vigtig! auditd er ikke version styring, og holder derfor ikke styr på hvad der blev ændret \
audit systemet kan holde øje med hvilken bruger der har ændret hvilken filer, directory eller foretaget system kald. Men ikke hvad der blev ændret.**


## Instruktioner



1. **Ubuntu har som udgangspunkt ikke audit daemon installeret. Installer audit daemon med kommandoen <code>apt install auditd</code></strong>
2. <strong>Verificer at auditd er aktiv med kommandoen <code>systemctl status auditd</code>. </strong>


![alt text](Billeder/Billede1.jpg)


    

![alt text](Billeder/Billede2.png)


3. **brug værktøjet <code>auditctl</code> til at udskrive nuværende regler med </strong>

    <strong>kommandoen<code>auditctl -l</code> </strong>


![alt text](Billeder/Billede3.jpg)


4. **Udskriv log filen som findes i <code>/var/log/audit/audit.log</code> </strong>


![alt text](Billeder/Billede4.jpg)


<strong>Hvis loggen virker uoverskuelig, har du det tilfælles med næsten resten af verden. Derfor bruger man typisk to værktøjer benævnt <code>ausearch</code> og <code>aureport</code> til at udlæse konkrette begivenheder. Begge værktøjer arbejdes der med i de kommende opgaver.</strong>

**At arbejde direkte med auditd er en smule primitivt ift. til mange andre værktøjer. Men det giver en forståelse for hvordan der arbejdes med audit på (næsten) laveste nivevau i Linux. Mange større og mere intuitive audit systemer (F.eks. SIEM systemer) benytter sig også af auditd, eller kan benytte sig auditd loggen.**



![alt text](Billeder/Billede5.png)



# Opgave 27 - audit en file for ændringer


## Information

**For at definerer begivenheder som udløser en "audit log" skal man opsætte en audit regel. Enten ved at tilføje reglen via værktøjet <code>auditctl</code> eller at tilføje en regel i audit regel filen <code>/etc/audit/audit.rules \
<em>At bruge loogen direkte er dog en smule uoverskueligt</em></code></strong>


## Instruktioner


### Tilføj audit regel med auditctl`

**1. Opret en audit regel for hver gang der skrives eller ændres på filen <code>/etc/passwd</code> med kommandoen <code>auditctl -w /etc/passwd -p wa -k user_change</code></strong>

**_Muligheden <code>-w</code> står for <code>where</code> og referer til den file hvor audit reglen skal gælde. <code>-p</code> står for <code>permission</code> og fortæller hvilken rettigheder der skal overvåges. I eksemplet står der <code>wa</code> hvilket står for attribute og write. Hvis filens metadata ændres, eller der skrives til filen, udløser det en begivenhed og der bliver oprettet et audit log</em></strong>

**2. udskriv en rapport over alle hændelser der er logget med kommandoen <code>aureport -i -k | grep user_change</code></strong>

**_Muligheden <code>-i</code> står for intepret, og betyder at numerisk entiter såsom bruger id bliver fortolket som brugernavn. <code>-k</code> står for key, hvilket betyder at regel der udløst audit loggen skal vises</em></strong>

**3. tilføj noget text i bunden af filen fra trin 1. \
4. Udfør trin 2 igen, og bekræft at der nu er kommet en ny række i rapporten \
_Der kommer en del nye rækker \
_5. Brug kommandoen <code>ausearch -i -k user_change</code>. Her skulle du gerne kunne finde en log som ligner den nedstående. \
</strong>

![alt text](Billeder/Billede6.jpg)

<strong> \
Generelt kan det være en smule rodet at skulle finde rundt i disse logs. Oftest bliver der produceret mere end en log når en begivenhed udløser en regel. Forskellen på <code>ausearch</code> og <code>aureport</code> er at ausearch viser flere oplysninger, men dermed også bliver mere uoverskuelig.</strong>



![alt text](Billeder/Billede7.png)





![alt text](Billeder/Billede8.png)




![alt text](Billeder/Billede9.png)



### Tilføj audit regel med audit regel konfigurations file

**Ulempen ved at tilføje regler direkte med auditctl er at regler ikke bliver persisteret. Altså de bliver ikke gemt og forsvinder når auditd eller hele systemet bliver genstartet. For at gemme reglerne skal man lave en regel file og gemme den i <code>/etc/audit/rules.d/</code></strong>

**1. Åben filen <code>/etc/audit/audit.rules</code>. Den skulle gerne se ud som vist nedenunder. </strong>


![alt text](Billeder/Billede10.jpg)
<strong> \
<em>regel filen bliver indlæst ved opstart af auditd og alle regler heri bliver de gældende audit regler</em></strong>

**Dette er auditd's primær konfigurations file, som indeholder alle regel konfigurationerne. men bemærk kommentaren øverst i filen. Den fortælle at denne file bliver autogeneret ud fra indholdet af <code>/etc/audit/audit.d</code> directory (ikke en synderlig intuitiv måde at lave konfigurations opsætning på). \
2. Opret en ny file som indeholder reglen, med kommandoen <code>sh -c "auditctl -l > /etc/audit/rules.d/custom.rules" \
<em>For at kommandoen virker skal reglen der blev lavet trin 1 forrige afsnit stadig være gældende, kontroller evt. med auditctl -l inden trin 2 udføres \
</em></code>3. Genstart auditd med kommandoen <code>systemctl restart auditd \
</code>4. Udskriv indholdet af <code>/etc/audit/audit.rules</code>. Resultatet bør ligne det som vises på billedet nedenunder. \
</strong>

![alt text](Billeder/Billede11.jpg)




![alt text](Billeder/Billede12.png)






![alt text](Billeder/Billede13.png)


**Slet filen /etc/audit/rules.d/custom.rules efter øvelsen**


### Audit alle file ændringer.

**Man kan også udskrive alle ændringer der er blevet lavet på overvåget filer.**



1. **Eksekver kommandoen <code>aureport -f</code>. Med denne bør du få en udskrift der ligner nedstående. \
</strong>


![alt text](Billeder/Billede14.jpg)


<strong>Den første række med nummeret 1. blev indsat 27 Marts 2023. kl. 20:11. begivenheden som udløste loggen var en udskrift af filen tmp.txt. udskrivning blev lavet af brugeren med id 1000. Og udskrivningen blev eksekveret med kommandoen <code>cat \
</code>2. Ændre kommandoen fra trin 1 således den skriver brugernavn i stedet for bruger id</strong>




![alt text](Billeder/Billede15.png)



# Opgave 28 - audit et directory


## Information

**Directories kan også overvåges med en audit log. Fremgangs måden er den samme som med filer.**

**De rettigheder man monitorer på (Read,write,attribute og execute) fungere på samme måde. Execute bliver udført når nogen prøver at skife sti ind i directoriet, F.eks. <code>cd /etc/</code></strong>


## Instruktioner



1. **Opret et nyt directory**
2. **Opret en audit regel med kommandoen <code>auditctl -w &lt;Directory path> -k directory_watch_rule \
<em>Bemærk at permission er bevist undladt</em></code></strong>
3. <strong>Brug auditctl til at udskrive reglen. Bemærk hvilken rettigheder der overvåges på. Dette er pga.´-p´ muligheden blev undladt.</strong>
4. <strong>Brug <code>chown</code> til at give <code>root</code> ejerskab over directory(fra trin 1), og brug <code>chmod</code> til at give root fuld rettighed og alle andre skal ingen rettigheder havde.</strong>
5. <strong>Med en bruger som ikke er root, eksekver kommandoen <code>ls &lt;Directory path></code> (directory er fra trin 1)</strong>
6. <strong>eksekver kommandoen <code>ausearch -i -k directory_watch_rule</code>. Dette resulter i en log som ligner nedstående. </strong>


![alt text](Billeder/Billede16.jpg)

<strong>ausearch er mindre læsbart end aureport, men indeholder tilgengæld mere information. Af en for mig ukendt årsag kan aureport ikke bruges til directory regler. Hvis man skal gennemgå større audit log filer hvor man leder efter noget specifikt, så kan <code>grep</code> kommandoen hjælpe med at filtre.</strong>



![alt text](Billeder/Billede17.png)



# Opgave 29 - audit OS api'et for specifikke system kald


## Information**Man kan monitorer alt kommunikation mellem kørende processer(applikationer) og operativ systemet. Dette gøres ved at monitorer <code>system kald</code>.</strong>


## Instruktioner[¶]


### monitorer processer som bliver slukket[¶]



1. **Tilføj reglen for <code>auditctl -a always,exit -F arch=b64 -S kill -F key=kill_rule</code></strong>
2. <strong>start en baggrunds proces med kommandoen <code>sleep 600 &</code></strong>
3. <strong>find proces id'et med <code>ps aux</code> på sleep processen </strong>

![alt text](Billeder/Billede18.jpg)

4. <strong>dræb processen med kommandoen <code>kill &lt;proces id></code></strong>
5. <strong>Eksekver kommandoen <code>aureport -i -k | grep kill_rule</code> og verificer at der er lavet nye rækker. \
<em>Formodentlig en del nye rækker, auditd har ofte sit eget liv</em></strong>



![alt text](Billeder/Billede19.png)



# Opgave 30 - checksum fra file[¶]


## Information[¶]

**For at sikre integriteten af data benytter man ofte en hash værdi som checksum. Man kan lave checksum af en enkelt file, eller afbilledinger af hele lageringsmedier.**

**En hash funktion bruges til at lave checksummer med. Hvis blot et enkelt karakter ændres i en hash funktions input, bliver outputtet helt anderledes.**

**En checksum sikre integriteten på data. Hvis en checksum har ændret sig, har data'en også ændret sig.**


## Instruktioner[¶]



1. **Installer <code>hashalot</code> med <code>apt</code>.</strong>
2. <strong>Opret en file som indeholder teksten <code>Hej med dig</code>.</strong>
3. <strong>Lav en checksum af filen med kommandoen <code>sha256sum &lt;path to file></code>.</strong>
4. <strong>Lav endnu en checksum af filen, og verificer at check summen er den samme.</strong>
5. <strong>Tilføj et <code>f</code> til teksten i filen.</strong>
6. <strong>Lav igen en checksum af filen.</strong>
7. <strong>Verificer at checksum nu er en helt anden.</strong>




![alt text](Billeder/Billede20.png)


**Efterforskningsprocess opgave:**



1. **Detektion af unormal aktivitet: **Efterforskningen begynder typisk med detektion af unormal aktivitet, enten gennem automatiserede overvågningsværktøjer, alarmer eller rapporter fra brugere om mistænkelig adfærd eller uregelmæssigheder på systemet. (**SIEM)**
2. **Indledende analyse: **Efterforskerne analyserer den indledende information for at fastslå, om der er indikationer på, at værten er blevet kompromitteret, og om der er opnået forhøjede privilegier. Dette kan omfatte at undersøge logfiler, overvågningsdata, systemkonfigurationer og andre relevante oplysninger.
3. **Identifikation af kompromitterede ressourcer: **Efterforskerne identificerer de ressourcer på værten, der er blevet kompromitteret, herunder filer, processer, brugerkonti og netværksforbindelser.
4. **Analyse af angrebsmetode: **Efterforskerne analyserer angrebsmetoden for at fastslå, hvordan angriberen har fået adgang til værten og opnået forhøjede privilegier. Dette kan omfatte undersøgelse af malware, udnyttelse af sårbarheder, phishing-e-mails eller andre taktikker, der er blevet anvendt.
5. **Undersøgelse af privilegieeskalerationsmetoder: **Efterforskerne undersøger de metoder, som angriberen har brugt til at eskalere deres privilegier på værten, herunder udnyttelse af sårbarheder i operativsystemet eller applikationer, svage adgangskontroller eller kapring af legitime brugerkonti.
6. **Bevissamling og dokumentation: **Efterforskerne indsamler og dokumenterer beviser relateret til kompromitteringen og de opnåede forhøjede privilegier, herunder logfiler, malware-eksemplarer, n**etværkstrafikdata og eventuelle andre relevante oplysninger.**
7. **Analyse af angrebsmønstre: **Efterforskerne analyserer angrebsmønstrene for at identificere eventuelle mønstre eller tendenser, der kan indikere, hvordan angrebet blev udført, og om det er relateret til tidligere kendte angreb eller angrebsgrupper.
8. **Forensisk undersøgelse: **Hvis det er nødvendigt, udføres en forensisk undersøgelse af værten for at identificere og analysere eventuelle skadelige artefakter og spor efter angrebet, som kan hjælpe med at fastslå angrebsvektor og angriberens identitet.
9. **Rapportering og respons: **Efterforskerne udarbejder en rapport baseret på deres undersøgelse og anbefaler passende responsforanstaltninger, herunder fjernelse af malware, rettelse af sårbarheder, styrkelse af sikkerhedsforanstaltninger og eventuel indberetning til retshåndhævende myndigheder eller relevante myndigheder.