# Opgave 11 - Brugerkontoer med svage passwords¶
## Information¶
Selv når der anvendes en stærk hash algoritme. Så yder hashet kun begrænset beskyttelse til svage kodeord. Dette skal der eksperimenteres med i denne opgave.

I opgaven skal der oprettes en bruger med et svagt kodeord. Herefter skal du  trække kodeord hashet ud fra shadow filen, og forsøge at genskabe passwordet ud fra hash værdien. 

## Instruktioner¶
**Alle kommandoer skal eksekveres mens du er i dit hjemme directory** 

 1. installer værktøjer john-the-ripper med kommandoen sudo apt install john

 2. Her efter skal du downloade en wordlist kaldet rockyou med følgende kommando wget https://github.com/brannondorsey/naive-hashcat/releases/download/data/rockyou.txt.

 3. Opret nu en bruger ved navn misternaiv og giv ham passwordet password

 4. Hent nu det hashet kodeord for misternaiv med følgende kommando sudo cat /etc/shadow | grep misternaiv > passwordhash.txt

 5. udskriv indholdet af filen passwordhash.txt, og bemærk hvilken krypterings algoritme der er brugt.

    $y$ -> yescrypt

 6. Eksekver nu kommandoen john -wordlist=rockyou.txt passwordhash.txt.

 7. Kommandoen resulter i No password loaded. Dette er fordi john the ripper værktøjet ikke selv har kunne detekter hvilken algoritme det drejer sig om.

 8. Eksekver nu kommandoen:   john --format=crypt -wordlist=rockyou.txt passwordhash.txt.

    format fortæller john the ripper hvilken type algoritme det drejer sig om

 9.  Resultat skulle gerne være at du nu kan se kodeordet, som vist på billede nedenunder.

![alt text](Billeder/136-1.png)

1. gentag processen fra trin 1 af. Men med et stærkt password.(minnimum 16 karakterer, både store og små bogstaver, samt special tegn)  
2. Reflekter over hvorfor komplekse kodeord er vigtige (Og andre gange en hæmmesko for sikkerheden).  

Der er 14 millioner passwords i listen og scriptet kører dem alle sammen igennem.

Ved at have et svært password, så er de først og fremmest ikke i listen og hvis det var tog det lægere tid at finde.. 

![alt text](Billeder/136-2.png)

På ~3 min nåede vi til linje ![alt text](Billeder/136-3.png)

Så det vil tage meget lang tid at komme igennem de 14 millioner linjer