# Opgave 9 - Bruger kontoer i Linux.
## Information¶
I de følgende opgaver skal der arbejdes med oprettelse, ændring og fjernelse af brugere i Linux-systemet.

I Linux er det kun brugere med superbrugerrettigheder, der kan  foretage ændringer i forhold til brugerkonti på Linux-systemet. Men da  emnet 'Brugerrettigheder' endnu ikke er blevet introduceret, vil der  først være en kort gennemgang af superbrugersystemet i Linux. Formålet  er, at du forstår, hvordan kommandoen 'sudo' kan anvendes.  

Husk og noter alle jeres besvarelser jeres cheatsheet

## Baggrunden for sudo-kommandoen¶
Brugersystemet i Linux (og andre operativsystemer) har til formål at  segmentere enkelte brugeres rettigheder, således at hver enkelt bruger  kun kan udføre arbejde på operativsystemet inden for den enkelte brugers  bemyndigelsesramme (Tænk: principle of least privilege). Derudover  giver brugersystemet mulighed for at sikre sporbarhed i forhold til  hvilke operationer den enkelte bruger har udført på operativsystemet  (Accountability, repudiation, audit). Derfor bør der for hver fysisk  bruger, der tilgår et Linux-system, eksistere en særskilt brugerkonto.  En brugerkonto i Linux omtales ofte som en login.

Linux-brugersystemet har som minimum altid én bruger kaldet "root",  som også kan benævnes som "superbrugerkontoen". Root-kontoen har adgang  til alt i et Linux-system, hvilket betyder, at den kan læse, slette,  oprette og redigere alle filer samt mapper. Root-kontoens login kan og  bør dog deaktiveres.

Det er dog ikke hensigtsmæssigt med kun én superbrugerkonto. Med  udgangspunkt i, at hver fysisk bruger altid skal tilgå et Linux-system  med en særskilt konto, opstår der en konflikt, såfremt mere end én  bruger har behov for superbrugerrettigheder. Sporbarheden i forhold til  den enkelte brugers handlinger i systemet vil ikke kunne opretholdes.  Derudover udgør en superbrugerkonto det, man kalder et "single point of  failure" i konteksten af det enkelte Linux-system. Det vil sige, at hvis  en konto, som har rettigheder til alt, bliver kompromitteret, er hele  systemet kompromitteret. Risikoen for at en konto bliver kompromitteret  gennem f.eks. passwordlæk eller lignende stiger, desto flere der har  adgang til passwordet.

For at løse udfordringen med superbrugerrettigheder til flere brugere  i Linux, bruger man "sudo" (Super user do), som er en kommando, der kan  bruges af alle, der er medlem af en gruppe kaldet "sudoers".

I Ubuntu er den bruger, man indledningsvist opretter (typisk under installationen), altid medlem af en gruppe kaldet "sudoers".

For at eksekvere en kommando med superbrugerrettigheder, eksekveres kommandoen således: sudo <Kommando>.

## Kommandoer til oprettelse, ændring og fjernelse af brugere¶
 følgende Linux-kommandoer, som er linket til herunder, skal bruges til at udføre opgaven.
• useradd - opret en ny brugerkonto

userdel - slet en brugerkonto
usermod - ændring af en eksisterende brugerkonto
passwd - ændring af brugerens adgangskode (blandt andet)
su - skift til en anden brugerkonto
User management guide:

https://www.makeuseof.com/user-management-linux-guide/

## Instruktioner¶
I de følgende opgaver skal der løses en række opgaver relateret til bruger kontoer. Alle opgaver kan løses med en af de kommandoer som er referet overover. Ellers er  det naturligvis tilladt at bruge information søgning, med F.eks. google.

### Opret en ny bruger.¶
I denne opgave skal der oprettes en ny bruger ved navn Mandalorian.

![alt text](Billeder/133-1.png)

![alt text](Billeder/133-2.png)

### Tildel en bruger password¶
I denne opgave skal brugeren Mandalorian, havde tildelt et nyt password.

![alt text](Billeder/133-3.png)

### Skift bruger¶
I denne opgave skal du skifte din aktive brugerkonto til Mandalorian. 

![alt text](Billeder/133-4.png)

### Slet en bruger.¶
I denne opgave skal brugeren Mandalorian slettes, men på samme tid kigger vi en finurlighed i Ubuntu ift. ejerskab over filer, og sletning af bruger.

I Linux er alle filer ejet af en bestemt bruger. Typisk når en bruger opretter en file, får denne bruger også tildelt ejerskab over filen.

Med kommandoen ls <filenavn> -al kan du få vist en tilladelse for en specifik file. Som vist på billede nedenunder:

![alt text](Billeder/133-5.png)

 Formatet er som vist nedenunder

 |Ejerens rettigheder|-|Gruppens rettigheder|-|Alle andres rettigheder|Antal links til filen| Ejer af filen| tilknyttet gruppe | Forklaring på resten af kolonerne er bevidst undladt indtil videre

1. Med brugeren mandalorian, opret en file. (F.eks. tom tekst file) 
Brugeren vi har oprettet har ikke permissions til at gøre noget som helst   

   https://unix.stackexchange.com/questions/548002/allow-all-users-to-create-files-in-a-directory-but-only-the-owner-can-delete: 

Alle brugere har adgang til /tmp, så vi gjorde det bare derinde

Kali opretter ikke selv et /home directory når man laver en ny bruger. Alternativt kunne man tilføje et flag til useradd commanden, så

![alt text](Billeder/133-6.png)

1. Skift til en anden bruger og slet brugeren Mandalorianed userdel. (Kan drille hvis der er en process som holder fast i brugeren. En genstart kan løse det for dig)
   

![alt text](Billeder/133-7.png)
1. Find en af de filer som blevet oprettet af brugeren Mandalorian
2. Se rettighederne for denne file, og noter hvem der er file ejer(Et id?).

![alt text](Billeder/133-8.png)
1. Opret en bruger ved navn Ivan
2. Se igen rettighederne for den file der blev oprettet af MandalorianHvem er ejeren nu?
   

![alt text](Billeder/133-9.png)
1. Så efter den tidligere ejer af en fil er blevet slettet, så bliver den næste bruger der bliver oprettet ejeren  

![alt text](Billeder/133-10.png)

1. Vurder om dette er en potetienelle sårbarhed, og om man bør overeje altid at slette/skifte ejerskab på filer når en bruger slettes.

Det er en meget stor sårbarhed, da hvis den slettede bruger havde ejerskab over en masse filer, så kan de blive overført til en ny bruger. 

**Et alternativ til at slette bruger kontoer, er at deaktiver dem**

Overvej om princippet Secure by default reelt er overholdt