# Opgave 9.1 - Bruger konto filer i Linux.¶
## Information¶
På Linux systemer er alle operativ systemets lokale brugerkontoer opbevaret i filer. Det  skal du kigge nærmer på i disse opgaver.

Husk og noter alle jeres besvarelser jeres cheatsheet

## Instruktioner¶
### Filer tilknyttet bruger systemet - opbevaring af bruger kontoer¶
I linux er der som udgangspunkt to filer som er intressant ift. bruger systemet. Den ene er filen passwd. I de fleste distributioner kan denne findes i stien /etc/passwd

1. asswd indeholder alle informationer om bruger kontoer(undtaget passwords).

    I denne opgave skal du udforske passwd filen.

    1. Se rettighederne for passwd med kommandoen ls <filenavn> -al   

![alt text](Billeder/135-1.png)

1. Overvej om rettighederne som udgangspunkt ser hensigtmæssige ud? og hvorfor der er de rettigheder som der er.

    User kan read og write. group og other kan reade filen. Det giver vel mening, hvis man skal bruge filen til noget logins?    

2. Udskriv filens indhold Hvis du har glemt hvordan så kig i dit cheat sheet. øvelsen blev udført i opgave 7

3. Samhold filens indhold med nedstående format (BEMÆRK: ikke alle kolonner er nødvendigvis tilstede) 

Brugernavn:Password(legacy, ikke brugt længere derfor x istedet):Bruger id:Gruppe id:Bruger information:Hjemme directory:Default shell

![alt text](Billeder/135-2.png)

Det passer sammen 

### Filer tilknyttet bruger systemet - passwords¶
Filen shadow opbevarer passwords til alle bruger kontoer. Alle passwords i shadow er hashes, som sikring mod udvekommende adgang til bruger kontoer.

I opgaven skal du udforske filen shadow

1. Udskriver rettighederne for filen shadow 

![alt text](Billeder/135-3.png)

1. Overvej rettighederne i samhold med Principle of least privilege. Det ser ud til at den indeholder nogle mere private oplysninger 
2. Udskriv filens indhold. her skulle vi bruge sudo   

![alt text](Billeder/135-4.png)

1. Samhold filens indhold med nedstående format.

:brugernavn:password(hashet):Sidste ændring af password:Minimum dage ind password skift:Maksimum dage ind password skift:Varslings tid for udløbet password:Antal dage med udløbet password inden dekativering af konto:konto udløbs dato 

Nogen af jer er endnu ikke introduceret til password hashes, hvilket er okay. I skal blot notere i jeres cheatsheet at i kan finde en forklaring på hashes i shadow filen i denne opgave.

Følgende viser et eksempel på et hash i shadow filen.  $y$j9T$GfMsEAQ8t9EkjiOiDzVRA0$cWqq3SrEZED3EvJYMFD/G1TYn8lgWOaSM8IvjCeD4j2:19417

 formattet er som følger:

 $hash algoritme id$salt$has$

 Hash algoritmernes id kan man slå op. I eksemplet er y brugt. Det vil sige at hash algoritmen til at  generer hashen er yescrypt.