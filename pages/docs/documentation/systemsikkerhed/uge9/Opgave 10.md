# Opgave 10 - Bruger rettigheder i Linux.¶
## Information¶
I tidligere opgave blev konceptet bruger rettigheder kort introduceret. Det skal der arbejdes mere i dybden  med  i denne opgave.  

Bruger rettigheder i linux deles op i 3 segmenter. Ejeren af en files rettigheder. gruppen som er tilknyttet filens rettigheder, og til sidst alle andres  rettigheder med filen.

 Som udgangspunkt er der 3 rettigheder som kan tildeles en file, det er:

 - Read - Brugeren kan læse filen (og kopier den).

 - Write - Brugeren kan skrive og ændre i filen.  

 - Execute - Brugeren kan eksekveren filen som et program.   

Såfremt man ønsker at se rettigheder på en file kan dette gøres med kommandoen ls -l ![alt text](Billeder/134-1.png)

 I det overstående eksempel har ejeren af filen rettighed til at  læse,skrive og eksekver filen. Ingen andre end ejeren har rettigheder  til denne file.  

Ligeledes kan rettighederne for et directory ses med kommandoen ls -ld "Directory navn"
![alt text](Billeder/134-2.png)

 bostavet d som står forrest indikerer at  rettigheder gælder for et directory.

 Ejeren  har læse,skrive og eksekverings rettigheder.   Eksekverings rettigheden på et directory betyder at selv om man ikke har læse rettigheder, kan man stadig tilgå indholdet (Men ikke liste det med F.eks. ls kommandoen).  

kommandoen chmod som anvendes til at tildele rettigheder til en file.  Man kan sætte rettighederne med enten tal eller bogstaver. Hvis man bruger tal er rækkefølgen for rettigheds tildeling |Ejer|Gruppen|Alle andre|.

 Nedstående tabel viser hvilken værdi der bruges til rettigheds kombinationer.  

![alt text](Billeder/tabel1.png)

700

ugo

så user har 7 = rwx

de andre har 0

Hvis F.eks. vil give brugeren alle rettigheder, og ingen rettigheder til andre vil kombinationen være 700. Vil man give ejeren læse og skrive rettigheder, og gruppen læse rettigheder, og eksekverings rettigheder til alle andre er kombinationen 641.

Lidt mere intuitivt kan man bruge bogstaver. Her vælger man hvilket segment der skal tildeles en rettighed med et bogstav.

![alt text](Billeder/tabel2.png)

Herefter bruger man +/- operatoren til at tildele rettigheder jvf. en dertil bestemt karakter  

![alt text](Billeder/tabel3.png)

Eksempelvis vil kommandoen chmod go-rwx fratage gruppen og alle andre, alle deres rettigheder rettigheder.

Husk at forsætte med at noter i dit Linux cheat sheet

## Links til beskrivelse af kommandoerne i opgaven:¶
chmod

chown

Tilføjer (r)ead, (w)rite, (x)ecute rettigheder til (a)lle på filen test.txt

![alt text](Billeder/134-3.png)

## Instruktioner¶
I de følgende opgaver skal der arbejdes med tildele og fratagelse af rettigheder til filer og directories.

 Udfør alle opgaver ved at tildele med både tal og bogstaver

### Giv og fjern læse rettigheder til en file¶
I denne opgave skal oprettes en file, og gives læse rettigheder til alle andre. Fjern derefter rettigheden igen.

Læse rettigheder til a(lle)

![alt text](Billeder/134-4.png)

og fjerner dem igen:

![alt text](Billeder/134-5.png)

og med Ocatal mode:

![alt text](Billeder/134-6.png)

fjerne: 

![alt text](Billeder/134-7.png)

### Giv og fjern skrive rettigheder til en file¶
I denne opgave skal oprettes en file, og gives skrive rettigheder til gruppen. Fjern derefter rettigheden igen.

chmod g+w test.txt

chmod 020 test.txt

### Giv og fjern læse rettigheder til et directory¶
I denne opgave skal oprettes en directory, og gives læse rettigheder til alle andre. Fjern derefter rettigheden igen.

![alt text](Billeder/134-8.png)

o = other 

### Giv og fjern skrive rettigheder til et directory¶
I denne opgave skal oprettes en directory, og gives skrive rettigheder til gruppen. Fjern derefter rettigheden igen.

![alt text](Billeder/134-9.png)

chmod 020 testdir

### Giv og fjern eksekverings rettigheder til en file¶
I denne opgave skal oprettes en file, og gives eksekverings rettigheder til gruppen. Fjern derefter rettigheden igen.

..

### Ændre ejeren af en file.¶
I denne opgave skal ejerskabet over en file ændres til en anden bruger, med kommandoen chown.

User er ejeren af test.txt

![alt text](Billeder/134-10.png)

nu root er root ejeren af filen (user^ er gruppen) 

### Sårbarhed - For mange rettigheder¶
Reflekter over konsekvens ved for mange rettigheder. Hvorfor sker det nogen gange? Og hvorfor søger nogen ikke ondsindet bruger at få flere rettigheder?

Det kan være ret besværligt at håndtere rettigheder for mange brugere, så det er lettere bare at give alle brugere rwx