# Opgave 33 - Overvågning af filer i directory med Wazuh


## Information

Wazuh agenten kan overvåge ændringer i en file eller et helt directory. I denne opgave skal der opsættes overvågning på et directory ift. til ændringer i denne.

Når man opsætter overvågning i Wazuh, skal der oftest ændres i wazuh agentens konfiguration file `/var/ossec/etc/ossec.conf`. Strukturen af denne konfiguration file er af ældre dato og er i formatet XML. Inden man ændre i filen kan det godt betale sig og orientere sig om filens generelle opbygning ved at gå på opdagelse i den.

Når der i denne og de kommende opgaver benævnes _blokken_ menes der inden for de omklammerende tags Eksempelvis er _hej_ skrevet inde i _syscheck_ blokken: `&lt;syscheck>hej&lt;/syscheck>`

Da jeg i sin tid skrev disse øvelser, var Wazuh dokumentationen meget mangelfuld. Det er den ikke længere, og en næsten Equivalent til denne øvelse, kan findes i wazuh dokumentationen [her](https://documentation.wazuh.com/current/user-manual/capabilities/file-integrity/use-cases/reporting-file-changes.html).

**En forudsætning for denne og alle kommende øvelser, er at øvelserne eksekveres på en host med en aktiv Wazuh agent**


## Instruktioner


### Opsætning af Wazuh agent

_Alle kommandoer eksekveres på den overvåget host \
_1. opret directoriet `/home/SecretFolder \
`2. Åben Wazuh agentens konfiguration file i en tekst editor. Filen findes i `/var/ossec/etc/ossec.conf \
`3. I blokken `&lt;syscheck>` skal følgende block tilføjes `&lt;directories check_all="yes" report_changes="yes" realtime="yes">/home/SecretFolder&lt;/directories> \
`4. genstart wazuh agenten med kommandoen `systemctl restart wazuh-agent \
`5. opret filen `/home/SecretFolder/secretFile.txt`. \
6. Tilføj teksten `Bad mojo` til filen `/home/SecretFolder/secretFile.txt \
`7. Slet filen `/home/SecretFolder/secretFile.txt`


### Wazuh Dashboard



1. I Wazuh dashboards, går ind på _security events \
_
![alt text](Billeder/Billede1.jpg)

2. I søge felte skal der indtastes `rule.id:(550 OR 553 OR 554) \
`

![alt text](Billeder/Billede2.jpg)

3. Dette bør frembring 3 nye begivenheder i security alerts \



![alt text](Billeder/Billede3.jpg)





![alt text](Billeder/Billede4.png)



# Opgave 34 - Detekter forsøg på Sql inject angreb


## Information

Wazuh kan overvåge log filer fra applikationer. I Denne opgave skal applikations loggen fra web serveren apache overvåges for forsøg på _Sql injection_ angreb gennem URL'en.

Hvis du endnu ikke er introduceret til sql injection angreb (eller har glemt hvad det) så kan du få en introduktion [her](https://www.hacksplaining.com/exercises/sql-injection#/start)

Da jeg i sin tid skrev disse øvelser, var Wazuh dokumentationen meget mangelfuld. Det er den ikke længere, og en næsten Equivalent til denne øvelse, kan findes i wazuh dokumentationen her.


## Instruktioner


### Opsætning af Wazuh agent



1. Opdater apt databasen med kommandoen `apt update`
2. Installer web serveren apache med kommandoen `apt install apache2`
3. Verificer at apache er aktiv med kommandoen `systemctl status apache2`
4. test apache med kommandoen `curl http://&lt;ubuntu ip>` fra en anden host.
5. Konfigurer Wazuh agenten til at monitorer apache2 loggen `/var/ossec/etc/ossec.conf`

 &lt;ossec_config>

   &lt;localfile>

     &lt;log_format>apache&lt;/log_format>

     &lt;location>/var/log/apache2/access.log&lt;/location>

   &lt;/localfile>

 &lt;/ossec_config>

6. Genstart Wazuh agenten


### Udfør angreb

_Alle kommandoer eksekveres på den angribende host \
_1. curl -XGET "http://OVERVÅGET_HOST_IP/users/?id=SELECT+*+FROM+users"


### Wazuh dashboard



1. Gå til security events
2. Filtrer på `rule.id:31103`


![alt text](Billeder/Billede5.jpg)



# Opgave 34 - Detekter forsøg på Sql inject angreb


## Information

Wazuh kan overvåge log filer fra applikationer. I Denne opgave skal applikations loggen fra web serveren apache overvåges for forsøg på _Sql injection_ angreb gennem URL'en.

Hvis du endnu ikke er introduceret til sql injection angreb (eller har glemt hvad det) så kan du få en introduktion her

Da jeg i sin tid skrev disse øvelser, var Wazuh dokumentationen meget mangelfuld. Det er den ikke længere, og en næsten Equivalent til denne øvelse, kan findes i wazuh dokumentationen her.


## Instruktioner


### Opsætning af Wazuh agent



1. Opdater apt databasen med kommandoen `apt update`
2. Installer web serveren apache med kommandoen `apt install apache2`
3. Verificer at apache er aktiv med kommandoen `systemctl status apache2`
4. test apache med kommandoen `curl http://&lt;ubuntu ip>` fra en anden host.
5. Konfigurer Wazuh agenten til at monitorer apache2 loggen `/var/ossec/etc/ossec.conf`

 &lt;ossec_config>

   &lt;localfile>

     &lt;log_format>apache&lt;/log_format>

     &lt;location>/var/log/apache2/access.log&lt;/location>

   &lt;/localfile>

 &lt;/ossec_config>

6. Genstart Wazuh agenten


### Udfør angreb

_Alle kommandoer eksekveres på den angribende host \
_1. curl -XGET "http://OVERVÅGET_HOST_IP/users/?id=SELECT+*+FROM+users"


### Wazuh dashboard



1. Gå til security events
2. Filtrer på `rule.id:31103`



![alt text](Billeder/Billede6.png)

![alt text](Billeder/Billede7.png)

# Opgave 35 - Detekter forsøg på shellshock angreb


## Information

I forrige opgave (34) overvåget wazuh apache web serveren for forsøg på sql injection angreb. Wazuh kan også "ud af boksen" detekter andre angreb. I denne opgave er det angrebet "Shellshock" som skal detekteres. Et shellshock angreb er egentlig bare et "Command injection" angreb. Altså man forsøger at "inject" en shell kommando ind i en applikation, i dette tilfælde gennem headeren _User-Agent_. Forklaring på command injection angreb kan du finde her

Da jeg i sin tid skrev disse øvelser, var Wazuh dokumentationen meget mangelfuld. Det er den ikke længere, og en næsten Equivalent til denne øvelse, kan findes i wazuh dokumentationen her.


## Instruktioner


### Opsætning af Wazuh agent



1. Installer apache og overvåg loggen (Såfremt du ikke allerede har gjort dette i opgave34)


### Udfør angreb



1. Fra en angribende host(F.eks.), eksekver kommandoen `sudo curl -H "User-Agent: () { :; }; /bin/cat /etc/passwd" &lt;Den overvåget host-IP>`


### Wazuh dashboard



1. Gå til Security events.
2. filterer på: `rule.description:Shellshock attack detected`

    
![alt text](Billeder/Billede8.png)

![alt text](Billeder/Billede9.png)


# Opgave 36 - Detekter forsøg på ondsindet kommandoer


## Information

Wazuh har i de forrige opgaver kunne detekterer angreb udfra mønstre som Wazuh har defineret "ud af boksen", altså noget den kan, uden behov for yderlige applikationer, eller større rekonfigurationer. Dette er dog ikke altid tilfældet. Derfor kan audit applikationer såsom `Auditd` hjælpe.

I denne opgave skal der overvåges for eksekvering af tvivlsomme applikationer på hosten. Applikationen som der skal overvåges for er Netcat, en applikation en trussels aktør kan bruge til at åbne TCP eller UDP socket på hosten.

Da Wazuh agent som udgangspunkt ikke selv kan detekerer for denne applikation, bruges Auditd loggen istedet. Wazuh agenten kan så istedet overvåge audit loggen, og reager derudfra.

Inden du påbegynder denne øvelse, bør du havde `Auditd` installeret på den host som wazuh agenten overvåger.

Da jeg i sin tid skrev disse øvelser, var Wazuh dokumentationen meget mangelfuld. Det er den ikke længere, og en næsten Equivalent til denne øvelse, kan findes i wazuh dokumentationen her.


## Instruktioner

For at dektekteringen kan blive aktiveret, skal Wazuh agenten på den overvåget host først konfigureres, herefter skal Wazuh server konfigureres til at reager på hændelser hvor mistænksomme applikationer eksekveres, F.eks. Netcat.


### Opsætning af Wazuh agent



1. tilføj følgende to audit regler i bunden af Auditd's regel file `/etc/audit/audit.rules`: \
`-a exit,always -F auid=1000 -F egid!=994 -F auid!=-1 -F arch=b32 -S execve -k audit-wazuh-c \
-a exit,always -F auid=1000 -F egid!=994 -F auid!=-1 -F arch=b64 -S execve -k audit-wazuh-c`
2. Genstart Auditd med kommandoen `sudo auditctl -R /etc/audit/audit.rules`
3. Kontroller at reglerne er indlæst med kommandoen `sudo auditctl -l`
4. Under kommentaren `&lt;!-- Log analysis -->` i Wazuh agenten's konfigurations file, tilføj følgende:

    &lt;localfile>

5.

     &lt;log_format>audit&lt;/log_format>

6.

     &lt;location>/var/log/audit/audit.log&lt;/location>

7.

    &lt;/localfile>

8.
9. Genstart Wazuh agenten med kommandoen `sudo systemctl restart wazuh-agent`


### Opsætning af Wazuh server



1. Åben CLI'en på Wazuh serveren.
2. Opret en ny file ved navn `suspicious-programs` i directoriet `/var/ossec/etc/lists/`
3. Åben filen `suspicious-programs` med nano, og tilføj følgende linjer:

    ncat:yellow

4.

    nc:red

5.

    tcpdump:orange

6.
7. Tilføj blokken `&lt;list>etc/lists/suspicious-programs&lt;/list>` til konfigurations filen `/var/ossec/etc/ossec.conf` i blokken `ruleset` for at generer et regelsæt

![alt text](Billeder/Billede10.jpg)


8. Opret en regel ved at tilføje følgende blok til filen `/var/ossec/etc/rules/local_rules.xml`

    &lt;group name="audit">

9.

     &lt;rule id="100210" level="12">

10.

         &lt;if_sid>80792&lt;/if_sid>

11.

     &lt;list field="audit.command" lookup="match_key_value" check_value="red">etc/lists/suspicious-programs&lt;/list>

12.

       &lt;description>Audit: Highly Suspicious Command executed: $(audit.exe)&lt;/description>

13.

         &lt;group>audit_command,&lt;/group>

14.

     &lt;/rule>

15.

    &lt;/group>

16.
17. genstart Wazuh manager med kommandoen `sudo systemctl restart wazuh-manager`


### Udfør angreb.


### På den overvåget host, eksekver kommandoen `sudo apt install netcat`



1. Eksekver herefter kommandoen `nc -v`
2. Gå ind under `Security events` på Wazuh dashboard og filterer med `data.audit.command:nc`


# Opgave 37 - Wazuh skanning for sårbar software.


## Information

Alle Wazuh opgaverne indtil videre, stammer fra Wazuh dokumentationen _Wazuh proof of concept_. \
En af de allervigtigste kompetencer man kan få inden for IT, er evnen til at lære nye technologier, koncepter osv. \
Derfor skal der i denne opgave implementeres sårbarheds skanning, men det skal gøres jvf. Wazuh's egen dokumentation.


## Instruktioner



1. Følg guiden til opsætning af sårbarheds skanning. _Den første fuld skanning af hosten kan godt tage lidt tid_

Dette gør: It performs vulnerability detection scans periodically for applications and operating systems on each monitored endpoint.



![alt text](Billeder/Billede11.png)