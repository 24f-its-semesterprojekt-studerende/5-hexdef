# Opgave 13 - rsyslog introduktion

Her er teksten med nogle korrektioner:


---


## Information

De fleste Linux-distributioner i dag har to daemons (applikationsprocesser), som logger parallelt med hinanden. Der er rsyslogd og journald. Den praktiske forskel mellem de to logdaemons er, at rsyslogd logger i plain text filer, hvorimod journald logger i binære filer.

I faget systemsikkerhed arbejdes der primært med rsyslog (i Ubuntu Linux).

Formålet med følgende øvelser er at introducerer Rsyslog konfigurations filerne.


## Instruktioner


### Opsætning af `locate` til søgning.

Kommandoen `find` er god til søgning af filer, men `locate` kan også med fordel anvendes.



1. Installer locate med kommandoen `sudo apt install locate`.
2. Opdater "Files on disk" databasen med `sudo updatedb`.


### Dan overblik over Rsyslog logfilerne på operativsystemet.



1. Brug `locate` til at finde alle filer med ordet `rsyslog`.
2. Dan dig et generelt overblik over filerne. Er der mange tilknyttede filer?


### Rsyslog konfigurationsfilen.

Rsyslog konfigurationsfilen indeholder den generelle opsætning af rsyslog daemon, herunder hvem der ejer logfilerne, og hvilken gruppe der er tilknyttet logfilerne. Herudover har den modulopsætning. Moduler er ekstra funktionaliteter, som man kan tilføje til rsyslog.



1. Brug locate til at finde rsyslog filen `rsyslog.conf`.
2. Åbn filen med `nano`.
3. I konfigurationsfilen, find afsnittet "Set the default permissions for all log files".
4. Noter, hvem der er filens ejer, og hvilken gruppe logfilerne er tilknyttet.
5. Udforsk de andre områder af filen.





![alt text](Billeder/Billede1.png)



# Opgave 14 - Logging regler for rsyslog kan ændres


## Information[¶](https://24f-its-syssec-ucl-pba-its-3db254c7a2dc5a246e5621c25301b2b971d3.gitlab.io/exercises/14_Linux_rsyslog_rules/#information)

Reglerne for, hvad rsyslog skal logge, findes i filen 50-default.conf.

Reglerne er skrevet som facility.priority action.

Facility er programmet, der logger. For eksempel: mail eller kern. Priority fortæller systemet, hvilken type besked der skal logges. Beskedtyper identificerer, hvilken prioritet den enkelte log har. Nedenfor er beskedtyper listet i prioriteret rækkefølge, højeste prioritet først:

emerg. alert. crit. err. warning. notice. info. debug. Således er logbeskeder af typen emerg vigtige beskeder, der bør reageres på med det samme, hvorimod debugbeskeder er mindre vigtige. Priority kan udskiftes med wildcard *, hvilket betyder, at alle beskedtyper skal sendes til den fil, der er defineret i action.

Formålet er følgende øvelse er at introducere opsætningen som Rsyslog anvender til at skrive log linjer fra specifikke applikationer og processer, til specifikke log filer.


## Instruktioner



1. i ryslog directory, skal filen `50-default.conf` findes.
2. Åben filen `50-default.conf`
3. Dan et overblik over alle de log filer som der bliver sendt beskeder til.
4. Noter hvilken filer mail applikationen sender log beskeder til, ved priorteringen _info_ , _warn_ og _err_




![alt text](Billeder/Billede2.png)



# Opgave 15 - Logrotation i Linux


## Information

Når der løbende bliver indsamlet logs, vil logfiler typisk kunne blive meget store over tid og optage meget plads på lagermediet. For at undgå dette kan man rotere logs i et givet tidsrum. For eksempel kan en log roteres hver 3. uge. Hvis logfilen `auth.log` roteres, vil den skifte navn til `auth.log.1`, og en ny fil ved navn `auth.log` vil blive oprettet og modtage alle nye logs. `auth.log.1` bliver en såkaldt _backlog_-fil, som kan flyttes til arkiv hvor filen kan arkiveres (Eller filen kan slettes hvis _rentention_ tiden er overskredet).

Formålet med denne øvelse er at demonstrer hvordan grundlæggende log rotation fungerer med Rsyslog.


## Instruktioner

I disse opgaver skal der arbejdes med logrotation i Linux (Ubuntu).

Du kan finde hjælp i Ubuntu man page til logrotate.



1. Åben filen `/etc/logrotate.conf`.
2. Sæt logrotationen til daglig rotation.
3. Sæt antallet af backlog-filer til 8 uger.





![alt text](Billeder/Billede3.png)



# Opgave 16 - Nedlukning af logservice


## Information

Loggingdaemons såsom Rsyslog kan lukkes ned på lige fod med andre applikationsprocesser i Linux. Formålet med denne øvelse er at demonstrere, at der kan slukkes for loggingen på den enkelte vært.

En modstander kan udnytte dette, når denne har opnået privilegeret adgang. Dette vil blokere for senere efterforskning af hændelser på værten.


## Instruktioner



1. Eksekver kommandoen `service rsyslog stop`. Efter dette vil der ikke længere blive genereret logs i operativsystemet.
2. Eksekver kommandoen `service rsyslog start`

    


![alt text](Billeder/Billede4.png)




# Opgave 18 - Applikationlogs[¶](https://24f-its-syssec-ucl-pba-its-3db254c7a2dc5a246e5621c25301b2b971d3.gitlab.io/exercises/18_Linux_application_logs/#opgave-18-applikationlogs)


## Information

I denne opgave skal der arbejdes med applikationlogs. Applikationlogs er de logfiler der anvendes af en applikation som eksekveres på hosten. Applikationen som der skal læse log filer fra, er web serveren Apache2, og log filen der skal læses er adgangsloggen som blandt andet viser hvilken ip adresser der har tilgået serveren. Dagens opsætning er en relativ simple opsætning. Formålet er blot at vise hvordan appliaktions logs fungerer, samt at applikationlogs kan opbevares som tekst filer på hosten.

I opgaven skal du først følge en guide som viser opsætningen af en Apache2 web server. Herefter skal du valider at serveren virker, ved at tilgå server fra anden host. Til sidst skal du verificerer at du kan se en log linje i apache2 applikationloggen for adgang.

Du kan få et fuldt overblik over apache2 logning [her](https://www.loggly.com/ultimate-guide/apache-logging-basics/) Og du kan se lokationen af Apache2 log filer på Ubuntu, i afsnittet [Application logs, her.](https://help.ubuntu.com/community/LinuxLogFiles)


## Instruktioner



1. Følg guiden for opsætning af Apache2 web server her [her](https://ubuntu.com/tutorials/install-and-configure-apache#1-overview)
2. Udskriv indholdet af apache's adgangs log som findes i `/var/log/apache2/access.log`.
3. Tilgå hjemmesiden fra en maskine der ikke er host maskinen.(Guiden viser eksempel. Default port for Apache server er port 80)
4. Verificerer at den sidste log i filen viser at den sidste maskine som har




![alt text](Billeder/Billede5.png)


    tilgået web serveren er den som du brugte til at udføre _trin 3_

5. Eksekver kommandoen `tail -f access.log`
6. Udfør _trin 3_ et par gange igen, og verificer at der kommer en ny log entry hver gang.

Mange af de tidligere opgaver har været centeret om log filer fra operativ systemet. Men det er meget vigtig at huske, at applikationer ofte har sine egene log filer (og autentificering). Og at hver applikation eksekveres med et sæt bruger rettigheder.




![alt text](Billeder/Billede6.png)



# Opgave 18.1 - Eftermiddagsopgave med webserver på Proxmox-serveren.


## Information

Til semesterprojektet forventes det, at der eksponeres to webservere mod DMZ. I denne eftermiddagsopgave skal I lave en Ubuntu-serveropsætning med Apache-webserver og en med NGINX-webserver. Til enten Apache-webserveren eller NGINX kan I bruge den Ubuntu-serverinstans, I tidligere har sat op, med navnet _Targethost_. Derudover skal I lave endnu en Ubuntu-server-VM, der kan hoste den webserver, som ikke blev sat op. Altså skal der være: - To Ubuntu-serverinstanser (Hvoraf den ene allerede er sat op tidligere) - På én instans skal der være en opsætning med Apache2-webserver - På den anden instans skal der være en opsætning med NGINX-webserver - Begge instanser skal overvåges af en Wazuh-agent - Begge instanser skal have individuelle brugerkonti til alle gruppemedlemmer.


## Instruktioner



1. Lav opsætning med Apache2-webserver på en Ubuntu-server-VM-instans. Du kan følge guiden her
2. Lav opsætning med NGINX-webserver på en anden




![alt text](Billeder/Billede7.png)


3. Ubuntu-server-VM-instans. Du kan følge guiden her
4. Lav individuelle brugerkonti til hvert gruppemedlem.



![alt text](Billeder/Billede8.png)


5. Lav opsætning, så begge Ubuntu-VM-instanser er overvåget.

Når opsætningen er færdig, skal I selvstændigt finde de logfiler, som bliver anvendt af NGINX. Der er en adgangslog og en fejllog.




![alt text](Billeder/Billede9.png)


Lav en tabel hvor i kan se hvilken VM instanser i har nu: F.eks. Opensense, Wazuh Server, Ubuntu server med Apache2, Ubuntu server med NGINX. I kan se et eksempel på dette i [Eftermiddags opgaven fra uge 9](https://24f-its-syssec-ucl-pba-its-3db254c7a2dc5a246e5621c25301b2b971d3.gitlab.io/exercises/Semester_projekt_UbuntuServerer/). Sikre jer at alle jeres Promox VM'er er noteret der, samt hvilken rolle de har (Router,webserver,SIEM osv. osv.)