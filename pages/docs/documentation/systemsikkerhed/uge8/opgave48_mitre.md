## Opgave 48 - (Gruppeopgave) Mitre ATT&CK Taktikker, Teknikker, mitigering & detektering

## Information  
Formålet med denne øvelse er at udforske Mitre ATT&CK-rammeværket med fokus på taktikken Privilege Escalation (Forhøjelse af Privilegier), teknikken T1548, samt afhjælpningen M1026 & Detekteringen DS0022.

Opgaven er en eftermiddagsopgave, og det forventes, at gruppen laver undersøgelsen sammen.

**I bliver kastet ud i den dybe ende med denne opgave. Det er meningen med øvelsen (Det og naturligvis at I skal lære Mitre at kende)**

https://attack.mitre.org/datasources/DS0017/ 


## Instruktioner
1) **Undersøg Mitre ATT&CK taktikken TA0004. Hvad betyder denne taktik? - https://attack.mitre.org/tactics/TA0004/**  
Privilege Escalation

Handler om teknikker for at kunne forhøje sine privilegier på et system eller netværk, her leder man typisk efter system svagheder og fejlkonfigurationer.

2) **Identificer specifikke under-teknikker under T1548 og hvordan de bidrager til at opnå Privilege Escalation. - https://attack.mitre.org/techniques/T1548/**   

**T1548.001 Setuid and Setgid:** Når Setuid- og Setgid-bits er indstillet på en eksekverbar fil, kan en angriber udnytte dette til at køre et program med privilegierne for ejeren af filen (Setuid) eller gruppen af filen (Setgid), uanset hvilken bruger der rent faktisk kører programmet. Dette giver angriberen mulighed for at opnå højere privilegier end dem, de normalt har. 

**T1548.002 Bypass User Account Control:** Windows User Account Control (UAC) allows a program to elevate its privileges (tracked as integrity levels ranging from low to high) to perform a task under administrator-level permissions, possibly by prompting the user for confirmation.
If the UAC protection level of a computer is set to anything but the highest level, certain Windows programs can elevate privileges or execute some elevated Component Object Model objects without prompting the user through the UAC notification box

**T1548.003: Sudo and Sudo Caching:** “sudo” er et værktøj, der giver brugere mulighed for at køre kommandoer med privilegierne for en anden bruger, ofte superbrugeren (root), hvis tilladelser er konfigureret korrekt. Dette er nyttigt i systemadministration for at tillade brugere at udføre specifikke opgaver, der normalt kræver højere privilegier, uden at give dem fuld root-adgang hele tiden. Hvis “sudo”-caching er aktiveret og konfigureret til en langvarig cache-tid, kan angribere udnytte denne mekanisme til at køre flere kommandoer med højere privilegier, selvom de kun behøver at indtaste adgangskoden en gang

**T1548.004 Elevated Execution with Prompt:**
Adversaries may leverage the AuthorizationExecuteWithPrivileges API to escalate privileges by prompting the user for credentials. The purpose of this API is to give application developers an easy way to perform operations with root privileges, such as for application installation or updating. This API does not validate that the program requesting root privileges comes from a reputable source or has been maliciously modified.

**T1548.005 Temporary Elevated Cloud Access:** Denne teknik refererer til situationer, hvor en angriber midlertidigt får adgang til skybaserede tjenester med højere privilegier end de normalt ville have, hvilket tillader dem at udføre ondsindede handlinger. Det kan ske ved: 
1. Forhøjelse af brugerrettigheder ved udnyttelse af sårbarheder i cloudtjenester fx ved at udnytte fejl i autentificeringsmekanismer.
2. Midlertidige adgangsnøgler som kan opfanges
3. Misbrug af midlertidige tilladelser


3) **Identificer og undersøg Mitigeringen M1026.**
Privileged Account Management: Handler om at kunne manage oprettelsen, modifikationen, brugen og permissions der er associeret med kontoer der har privilegier. Dette gælder “T1548, Abuse Elevation Control Mechanism”Hvilket omhandler at kunne fjerne users fra den lokale administrator gruppe på systemet. “ T1134 Access Token Manipulation” Hvilket omhandler at kunne modificere access tokens for at kunne bypass access kontroller

4) **Identificer og undersøg detekteringen DS0022.**
DS0022: File - Et objekt, som administreres af I/O-systemet, og som bruges til at opbevare data såsom billeder, tekst, videoer, computerprogrammer eller en bred vifte af andre medier.


5) Til næste uge, forbered en kort præsentation, der opsummerer dine resultater, herunder en forklaring på Privilege Escalation-taktikken, teknikkerne under T1548, mitigeringen M1026 samt detekteringen DS0022.
Det behøver ikke at være en stor og forkromet præsentation, blot noget i stil med "Daniel, gæstelæreren", når han præsenterer resultatet af hans øvelser. Altså maks 5 minutter

# Spørgsmål til diskussion i gruppen
1) **Hvad er formålet med Privilege Escalation-taktikken (TA0004) inden for Mitre ATT&CK-rammeværket, og hvorfor er det en vigtig del af en angribers taktikker?** 
For at den ondsindede aktør kan få root access/høje privileges og dermed let kan opnå deres mål

Formålet med Privilege Escalation er at øge angriberens kontrol og adgangsniveau, hvilket giver dem mulighed for at udføre mere skadelige handlinger, såsom at installere skadelig software, stjæle data, manipulere systemkonfigurationer eller udføre andre former for angreb.


2) **Hvordan bidrager teknikkerne under T1548 til angriberens evne til at opnå Privilege Escalation?**
De teknikker, der er under T1548 i MITRE ATT&CK-frameworket, har flere fællestræk, der bidrager til deres rolle i at opnå Privilege Escalation:

**Udnyttelse af sårbarheder eller svagheder:** Alle disse teknikker involverer udnyttelse af enten kendte sårbarheder, zero-day-sårbarheder eller konfigurationsfejl i systemer eller applikationer. Ved at udnytte disse svagheder kan en angriber opnå adgangsniveauer, der normalt ikke er tilladt.  

**Manipulation af tilladelser og adgangskontroller:** Disse teknikker involverer manipulation af tilladelser og adgangskontroller enten på operativsystemniveau, applikationsniveau eller gennem andre mekanismer som UAC på Windows-systemer. Dette tillader angriberen at hæve deres privilegieniveau.

**Brug af offentligt tilgængelige ressourcer eller tjenester:** Flere af disse teknikker udnytter offentligt tilgængelige ressourcer eller tjenester, såsom fjernservices eller offentligt tilgængelige applikationer. Ved at udnytte disse ressourcer kan en angriber få adgang til systemet med højere privilegier.

**Udnyttelse af autentificeringsmekanismer:** Flere af disse teknikker involverer udnyttelse af autentificeringsmekanismer for at få uautoriseret adgang til systemet eller for at opnå adgangsniveauer med højere privilegier. Dette kan omfatte omgåelse af UAC på Windows-systemer eller manipulering af autentificeringsmekanismer på fjernservices.


3) Hvad er karakteristika for mitigeringen M1026, og hvordan hjælper den med at afhjælpe risiciene forbundet med Privilege Escalation?
https://attack.mitre.org/mitigations/M1026/


Mitigation M1026 i MITRE ATT&CK-frameworket henviser til at implementere restriktive adgangskontroller og princippet om least privilege. Dette indebærer at begrænse brugernes adgang til systemer, netværk og data til kun de nødvendige ressourcer og handlinger, de har brug for for at udføre deres arbejdsopgaver. Når det gælder mitigering af risiciene forbundet med Privilege Escalation, har M1026 følgende karakteristika og fordele:

**Implementering af princippet om mindste privilegium (least privilege):** Mitigation M1026 fremhæver vigtigheden af at følge princippet om mindste privilegium. Dette indebærer at tildele brugere og processer de laveste adgangsniveauer, der er nødvendige for at udføre deres specifikke opgaver eller funktioner. Dette reducerer risikoen for, at en angriber kan udnytte højere privilegier for at få adgang til følsomme data eller udføre skadelige handlinger.

**Begrænsning af administrative privilegier:** Mitigation M1026 indebærer også begrænsning af administrative privilegier til kun de nødvendige brugere eller roller. Dette betyder, at kun autoriserede administratorer eller brugere får adgang til administrative funktioner eller værktøjer, hvilket reducerer risikoen for, at uautoriserede brugere kan opnå administrative privilegier gennem Privilege Escalation.

**Implementering af adgangskontroller:** Mitigation M1026 fokuserer på implementering af restriktive adgangskontroller, herunder brug af adgangskontrol-lister (ACL'er), rollebaseret adgangskontrol (RBAC) og andre sikkerhedsforanstaltninger til at begrænse adgangen til systemer, applikationer og data. Dette reducerer risikoen for, at uautoriserede brugere eller processer kan opnå forhøjede privilegier gennem svagheder i adgangskontrollerne.

**Regelmæssig gennemgang og opdatering:** Mitigation M1026 indebærer også regelmæssig gennemgang og opdatering af adgangskontroller, privilegieniveauer og brugerrettigheder for at sikre, at de er passende konfigureret og beskyttet mod misbrug eller Privilege Escalation.



