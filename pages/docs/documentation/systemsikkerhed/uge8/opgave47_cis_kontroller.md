Opgave 47 - (Gruppeopgave) CIS kontroller

I denne opgave skal gruppen lave vurderinger af en fiktiv virksomhed og dens IT-sikkerhedsmodenhedsniveau i henhold til risikoprofil og størrelse.

BS Consulting A/S er en mellemstor virksomhed, der opererer inden for teknologibranchen. De leverer softwareløsninger til kunder over hele verden og opbevarer følsomme kundedata på deres systemer. Virksomheden har været bekymret for deres cybersikkerhedspraksis og ønsker at forbedre deres modenhed på dette område. De har bedt om hjælp til at vurdere deres nuværende cybersikkerhedssituation og identificere, hvilken implementeringsgruppe (IG) de mest sandsynligt hører til.



1. BS Consulting A/S har etableret en IT-afdeling, der er ansvarlig for at håndtere deres cybersikkerhedsbehov.
2. De har implementeret antivirus- og antimalware-software på alle deres systemer og foretager regelmæssige opdateringer.
3. Der er en adgangskontrolpolitik på plads, som begrænser brugeradgang til systemer baseret på deres roller og ansvarsområder.
4. Der er en sikkerhedspolitik, men den er ikke blevet opdateret i over et år, og medarbejdere er ikke blevet trænet i overholdelse af denne politik.
5. Virksomheden har en reaktiv tilgang til cybersikkerhed, hvor de kun reagerer på hændelser, når de opstår, i stedet for at have proaktive sikkerhedsforanstaltninger på plads.

    Vurder BS Consulting A/S' nuværende cybersikkerhedspraksis og identificer, hvilken implementeringsgruppe (IG) BS Consulting A/S sandsynligvis hører til baseret på deres nuværende cybersikkerhedspraksis. Beskriv jeres begrundelse for vurderingen. 

1. **Hvordan vurderer du BS Consulting A/S' nuværende cybersikkerhedspraksis i forhold til de forskellige CIS 18 kontroller?**

        BS Consultings sikkerhedspraksis er mangelfuld grundet manglende implementering og forankring af sikkerhedspolitik. Medarbejderne ved ikke hvordan de skal forholde sig til sikkerhed og udgør derfor en stor risiko for virksomheden. Derudover har de en reaktiv tilgang til sikkerhed, hvilket betyder at de ikke er sikret med de rigtige foranstaltninger på potentielle hændelser. 


        Control 10: Malware defence - implementeret


        Control 17: Incident management  - ikke implementeret


        Control 06: Access control - Implementeret


        Control 14: Security awareness and skills training - ikke implementeret

2. **Hvilken implementeringsgruppe (IG) mener du, BS Consulting A/S hører til, og hvorfor?  \
 \
**IG - 2 Da de har en dedikeret IT afdeling med it eksperter til rådighed for at kunne implementere kontroller, samt har visse foranstaltninger implementeret og desuden behandler følsom klient data
* De har adgangskontrol (CIS control 6 Access Control Management)
* Antimalware/antivirus software (CIS Control 10 Malware defenses)

        Virksomheden mangler nogle afgørende punkter før at kunne blive en del af IG - 3, såsom deres sikkerhedspolitik/ Control 14

3. **Hvad kunne BS Consulting A/S gøre for at forbedre deres cybersikkerhedsmodenhed i forhold til deres risikoprofil og størrelse samt bevæge sig til en højere implementeringsgruppe?**

        BS Consulting bør sikre alle relevante kontroller i CIS implementeres for at øge cybersikkerhedsmodenheden. 


        Dedikerede funktioner der håndtere forskellige discipliner af sikkerhed: risk mgt., pen test, app sec osv. Tæt sammenspil mellem forskellige sikkerhedsfunktioner. 


        Virksomheden vurderes som IG2, men kan implementere flere foranstaltninger fra IG3. 


![alt text](billeder/cis_kontroller/Picture1.png)

![alt text](billeder/cis_kontroller/Picture2.png)