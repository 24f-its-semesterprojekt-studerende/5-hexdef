# Overview 


## Using XSTerm.js with Debian host (CLI only)
Using XSterm.js rather than noVnc, allows for pasting into to VM from  the host.
XSterm.js uses serial ports for communication, which requirements some additional 
configuration of the VM. You need to add a serial port to the VM, and setup
a serial port listener in the host.

### Add serial port to VM
Add a serial port to VM, by Hardware -> Add -> Serial port in the VMS proxmox menu.
The number of the serial port should be an unused serial port number (E.g. if serial 0 is used, assign 1 instead)

### Start serial port listener on VM
In the console of them VM, start a serialport listener
by executing following commands:
  
- `sudo systemctl enable serial-getty@ttyS0.service`
- `sudo systemctl start serial-getty@ttyS0.service`

## Using SPICE with desktop host (E.g. Kali)

## install Virt manager on host
On the host, install the application [Virt-manager](https://virt-manager.org/download.html).
Virt manager will interact with the promox VM from the host.

## Install vd-agent
vd-agent is software that makes the VM interacts more seeminglessly with virt-manager.
To install this,  exccute the command ` sudo apt-get install spice-vdagent`
_In some cases, you may need to execute sudo apt-get install software-properties-common first_

# Links
[Proxmox official guide for adding serial port to host](https://pve.proxmox.com/wiki/Serial_Terminal)

[Commands for starting serial port listener on Debian host](https://gist.github.com/edisonlee55/7fdd61ea9ed9aba68e2dd02f6ff4814a)