---
hide:
  - footer
  - toc
---

# UCL PBa IT Sikkerhed - Semesterprojekt gruppe 5 - HexDef

På dette website finder du dokumentation, læringslogs og andet relateret til semesterprojektet for gruppe 5 (HexDef) i IT Sikkerhed på UCL bestående af:

* Jens Schou Jensen - jsje37559@edu.ucl.dk (2. semester)
* Sebastian Ishmael Nielsen - sini56860@edu.ucl.dk (2. semester)
* Elias Gashi - elga54543@edu.ucl.dk (Dimplomstuderende)

Læringslogs og link til dokumentation vil blive inkluderet som bilag i projektrapporten (eksamensaflevering).  

Procesafsnittet i rapporten skal beskrive de enkelte ugers projektarbejde med henvisning til logs via links til relevante dele af denne gitlab side.

Semesterprojektet er beskrevet på: [https://ucl-pba-its.gitlab.io/24f-its-semester-projekt/](https://ucl-pba-its.gitlab.io/24f-its-semester-projekt/)
