## Uge 6

- Alle studerende har forståelse for fagets formål.
- Alle studerende kan (Nogenlunde)tolke læringsmålene for faget.
- Alle studerende har en fungerende Ubuntu Server VM. 

#### Læringsmål der arbejdes med i faget denne uge
_I denne uge arbejde vi ikke med konkrette læringsmål fra studieordningen, men forståelsen for dem

## Uge 7

- Alle studerende har kendskab til de grundlæggende Linux kommandoer.
- Alle studerende kan navigere i Linux file struktur.
- Alle studerende kan oprette og  slette filer i Linux
- Alle studerende kan oprette og slette directories i Linux
- Alle studernede kan fortag en søg på en file eller mappe i Linux
- Alle studerende kan identificer en proces
- Alle studerende kan "dræbe en proces"


**Overordnede læringsmål fra studie ordningen:**  
  
**Viden:**  
    - Relevante it-trusler (Oplæg)  
    - Relevante sikkerhedsprincipper til systemsikkerhed  
  
**Kompetencer:**  
    - Vi kan håndtere enheder på command line-niveau



## Uge 8

- Den studerende forstår hvad sikkerheds benchmarks kan bruges til
- Den studerende forstår hvad CIS benchmarks kan bruges til
- Den studerende forstår samehængen mellem CIS benchmarks og CIS Controls
- Den studerende forstår hvad Mitre ATT&CK databasen kan anvendes til.

  
**Overordnede læringsmål fra studie ordningen:**  
- **Viden:**  
  - Generelle governance principper / sikkerhedsprocedure  
  - Relevante it-trusler  
- **Færdigheder:**  
  - Vi kan følge et benchmark til at sikre opsætning af enheder.

## Uge 9

- Den studerende kan oprette, slette eller ændre en bruger i bash shell
- Den studerende kan ændre bruger rettighederne i bash shell
- Den studerende har en grundlæggende forståelse for principle of privilege of least
- den studerende har en grundlæggende forståelse for mandatory og discretionary access control.



**Overordnede læringsmål fra studie ordningen:**

- **Viden:** 
- Relevante it-trusler
- Relevante sikkerhedsprincipper til systemsikkerhed
- OS roller ift. sikkerhedsovervejelser
- **Færdigheder:**
- Udnytte modforanstaltninger til sikring af systemer
- **Kompetencer:**
- Vi kan Håndtere enheder på command line-niveau
- Vi kan Håndtere udvælgelse, anvendelse og implementering af praktiske mekanismer til at forhindre it-sikkerhedsmæssige hændelser



## Uge 10

- En forståelse for at hændelser i den fysiske verden også kan udgører en it-sikkerheds trussel.
- At den studerende kan lave en grundlæggende opsætning af en firewall.
- At den studerende har forståelse for firewall'ens rolle på en host.


**Overordnede læringsmål fra studie ordningen:**

- **Viden:**
  - Generelle sikkerhedprocedurer.
  - Relevante  it-trusler
- **Færdigheder:** ..
  - Udnytte modforanstaltninger til sikring af systemer
- **Kompetencer:** ..
  - Vi kan Håndtere værktøjer til at fjerne forskellige typer af endpoint trusler.
  - Vi kan Håndtere udvælgelse og anvend af praktiske til at forhindre it-sikkerhedsmæssige hændelser.

## Uge 11

- Den studerende kan redegøre for hvornår der som minimum bør laves en log linje
- Den studerende har grundlæggende forståelse for log management


**Overordnede læringsmål fra studie ordningen:**

- **Viden:**
- Generelle governance principper
- Relevante  it-trusler
- Relevante sikkerhedsprincipper til systemsikkerhed
- **Færdigheder:**
- implementere systematisk logning og monitering af enheder (Påbegyndt)
- Analyser logs for incidents og følge et revisionsspor (Påbegyndt)
- **Kompetencer:**
- Vi kan Håndtere udvælgelse af praktiske mekanismer til  at detektere it-sikkerhedsmæssige hændelser.

## Uge 12

Ugens emner er:

- Logning
- Logning i Linux

### Læringsmål

- Den studerende kan redegøre for hvad der skal logges
- Den studerende har kendskab til Ubuntu logging system
- Den studerende kan udføre grundlæggende arbejde med Ubuntu log system.

**Overordnede læringsmål fra studie ordningen:**

- **Viden:**
- Relevante  it-trusler
- Relevante sikkerhedsprincipper til systemsikkerhed
- **Færdigheder:** ..
- implementere systematisk logning og monitering af enheder (Påbegyndt)
- Analyser logs for incidents og følge et revisionsspor (Påbegyndt)
- **Kompetencer:** ..
- Håndtere udvælgelse af praktiske mekanismer til at dektekter it-sikkerhedsmæssige hændelser.