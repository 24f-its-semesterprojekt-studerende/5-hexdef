# Uge 06 - Introduktion til faget

Dette er første undervisningsgang i faget netværks- og kommunikationssikkerhed.   
Vi skal lære hinanden og faget at kende.  

## Emner

### Ugens emner er

- Introduktion til faget. 
- Grundlæggende netværk.
- Opsætning af værktøjer.

## Mål for ugen

Herunder kan du læse ugens forskellige mål

### Forberedelse

- Se forberedelse i planen på itslearning

### Praktiske mål

**Formiddag:**  

- Øvelse 00 
- Øvelse 10 
- Øvelse 20 

**Eftermiddag**

- Øvelse 21 
- Øvelse 22 
- Øvelse 30 

### Læringsmål der arbejdes med i faget denne uge

#### Overordnede læringsmål fra studie ordningen

- **_Den studerende har viden om og forståelse for_**  

    - Hvilke enheder, der anvender hvilke protokoller
    - Gængse netværksenheder der bruges ifm. sikkerhed (firewall, IDS/IPS, honeypot, DPI)

#### Læringsmål den studerende kan bruge til selvvurdering

- Den studerende har viden om hvad faget går ud på
- Den studerende har viden om grundlæggende netværk

# Uge 07 - OSI modellen og netværks protokoller 

Denne uge er endnu en uge med grundlæggende netværk. Vi skal arbejde en del med OSI modellen og finde både enheder samt protokoller og koble dem til de forskellige lag i modellen.  
Du får mulighed for at lære hvad OSI modellen kan bruges til i forhold til fejlfinding og også hvad den kan betyde i forhold til sikkerhed.  
Endelig byder ugens øvelser på masser af praktisk arbejde med både wireshark, opnsense, proxmox og nmap. Det giver mulighed for at få en dybere forståelse for hvordan netværksenheder og trafik opfører sig i praksis.  

Husk at i som gruppe skal dokumentere jeres arbejde, det er især vigtigt at i reflekterer over hvad i lærer af at lave øvelser og arbejde sammen i gruppen.  

Grundene til at det er vigtigt er flere. Dels fungerer det som feedback til mig så jeg kan se hvad der virker for jer og hvad der ikke gør. Vi er mange i klassen og jeg kan desværre ikke nå rundt og snakke direkte med jer alle sammen til hver undervisningsgang og derfor er jeres læringslog et rigtig vigtigt værktøj for mig.  

Det er også vigtigt at i selv kan se at i udvikler jer, det kan i også se igennem jeres logs.  

Endelig er der eksamensprojektet i slutningen af semestret. Alle de øvelser vi laver gennem semestret er nogle i får brug for at kunne gentage når i skal implementere systemet til brug i jeres semesterprojekt, derfor er det essentielt at i skriver gode guides som i kan følge senere.  

## Emner

- OSI modellen
- Netærksenheder
- Protokoller

## Forberedelse

- Se forberedelse i planen på itslearning

## Mål for ugen

Herunder kan du læse ugens forskellige mål

### Praktiske mål

**Formiddag:** 

- Øvelse 11
- Øvelse 12   
- Øvelsen 13

**Eftermiddag**

- Øvelse 23: Hvad er opnsense og hvad kan det
- Øvelse 40
- Øvelse 50

### Læringsmål der arbejdes med i faget denne uge

#### Overordnede læringsmål fra studie ordningen

- **_Den studerende har viden om og forståelse for_** 
    - Hvilke enheder, der anvender hvilke protokoller
    - Forskellige sniffing strategier og teknikker
    - Adressering i de forskellige lag
    - Gængse netværksenheder der bruges ifm. sikkerhed (firewall, IDS/IPS, honeypot, DPI).
    - Netværkstrusler

- **_Den studerende kan_**  
    - Identificere sårbarheder som et netværk kan have

- **_Den studerende kan håndtere udviklingsorienterede situationer herunder_** 
    - Monitorere og administrere et netværks komponenter

#### Læringsmål den studerende kan bruge til selvvurdering

- Den studerende kan forklare OSI modellen og de tilhørende lag
- Den studerende kan placere hardware på de forskellige lag i OSI modellen
- Den studerende kan forklare hvad en protokol er og hvordan den er opbygget

# Uge 08 - Netværksdesign, segmentering og firewalls

## Emner

Netværksdesign er en vigtig del af netværks- og kommunikations sikkerhed. I denne uge ser vi på hvordan man organiserer sig fysisk og logisk med netværk.

**Segmentering** er en praktisk og sikkerhedsmæssig inddeling af netværket.  
Segmentering begrænser broadcast trafik som kan blive en **flaskehals** i større netværk, men endnu vigtigere er at segmentering giver mulighed for at inddele servere, laptops, services og alt det andet der kommunikerer på vores netværk.  
Men hvorfor giver det øget sikkerhed?

Det gør det fordi segmentering giver mulighed for at lave mere end et **bolværk** der beskytter netværket.  
Hvert segment kan beskyttes af sin egen firewall og egne regler.  
På den måde bidrage til en **Defense in depth** strategi, hvor en angriber skal nedlægge mere end en firewall, for at få adgang til vitale dele af netværket.

Det vil altid være en **risikovurdering** der afgør hvilke del af netværket der skal passes bedst på og hvilke dele af netværket der anses som usikkert.

Som regel er det **tjenester** der vender mod internettet som webservere, mailsertvere osv. der anses som usikre.  
Andre dele af netværket, for eksempel **uddateret hardware** kan placeres på sit eget segment af den simple årsag at det ikke er muligt at sikkerhedsopdatere mere, men stadig er nødvendigt for forretningen.

På netværket vil der også være vitale servere som for eksempel dem der har ansvar for **autentificering** af brugere og enheder som skal beskyttes ekstra godt.  
De skal for eksempel adskilles fra både internet og uddateret hardware så meget som muligt.

I denne uge er fokus på at få **viden** og **færdigheder** i forhold til hvordan et netværk kan **segmenteres**, samt hvordan **firewalls** kan beskytte netværket.  
Begge dele i både teori og praksis.

Wireshark er stadig et rigtig godt værktøj og det anbefales at bruge det som et værktøj til både at forstå og **fejlfinde** på netværket (med andre ord, **brug wireshark når noget ikke virker istedet for at gætte og google....**)

### Ugens emner er

- Netværksdesign
- Segmentering
- Firewalls

### Materiale

## Mål for ugen

Herunder kan du læse ugens forskellige mål

### Praktiske mål

- Øvelse 41
- Øvelse 51
- Øvelse 60 
- Øvelse 52 
- Øvelse 61 

### Læringsmål der arbejdes med i faget denne uge

#### Overordnede læringsmål fra studie ordningen

- **_Den studerende har viden om og forståelse for_**
  - Sikkerhed i TCP/IP
  - Hvilke enheder, der anvender hvilke protokoller
  - Gængse netværksenheder der bruges ifm. sikkerhed (firewall, IDS/IPS, honeypot, DPI)
- **_Den studerende kan_**
  - Identificere sårbarheder som et netværk kan have
- **_Den studerende kan håndtere udviklingsorienterede situationer herunder_**
  - Designe, konstruere og implementere samt teste et sikkert netværk

#### Læringsmål den studerende kan bruge til selvvurdering

- Den studerende kan forklare fordele og ulemper ved forskellige netværks topologier
- Den studerende kan forklare segmentering, og dets relation til sikkerhed og netværksperformance
- Den studerende kender til hvordan netværksenheder bruges
- Den studerende kender forskellige typer af firewalls
- Den studerende kan opsætte et simpelt segmenteret netværk og konfigurere firewall regler mellem netværk

# Uge 09 - Designe og dokumentere netværk

Denne uge byder på øvelser i den systematik der er fuldstændig nødvendig for at kunne arbejde systematisk med design og vedligehold af et netværks sikkerhed.

IP Address Management (IPAM) introduceres som teoretisk begreb og den studerende får mulighed for at udføre IPAM i praksis gennem open source softwaren netbox.

I denne uge arbejdes der også med CIS18 benchmark til at hærde OPNsense routeren. Den studerende får kendskab til hvordan routeren kan hærdes gennem forskellige tiltag og skal også afprøve udvalgte foranstaltninger i praksis.

Endelig skal et netværk designes med sikkerhed gennem segmentering og firewalls. Øvelsen er dels rutine i grundlæggende netværk men også inspiration til hvordan der kan arbejdes med sikre netværk i eksamensprojetet.

## Emner

### Ugens emner er

- Dokumentation af netværk (IPAM m.m.)
- Beskyttelse af netværksenheder (CIS18 benchmarks)
- Designe netværk (træne at designe netværk med sikkerhed)

### Materiale

## Mål for ugen

Herunder kan du læse ugens forskellige mål

### Praktiske mål

- Øvelse 70 
- Øvelse 24 
- Øvelse 53 

### Læringsmål der arbejdes med i faget denne uge

#### Overordnede læringsmål fra studie ordningen

- **_Den studerende har viden om og forståelse for_**
  - Gængse netværksenheder der bruges ifm. sikkerhed (firewall, IDS/IPS, honeypot, DPI)
- **_Den studerende kan_**
  - Identificere sårbarheder som et netværk kan have
- **_Den studerende kan håndtere udviklingsorienterede situationer herunder_**
  - Designe, konstruere og implementere samt teste et sikkert netværk

# Uge 10 - Bygge sikrede netværk

I denne uge repeterer og konsoliderer vi hvad i indtil videre har berørt i de foregående uger, grundlæggende netværk, design, segmentering, firewalls, teste og dokumentere netværk. Det gør vi ved at i skal implementere og sikre et netværk som er designet i forvejen, samt udarbejde en plan for hvordan implementationen kontrolleres i fremtiden. 

Netværket i bygger denne uge danner basis for arbejdet de følgende uger og er et godt eksempel på hvordan i kan arbejde med netværkssikkerhed i eksamensprojektet. Det er derfor vigtigt at i alle deltager og får lavet øvelserne.

## Emner

### Ugens emner er

- Forskellige typer af netværks logs
- Implementering af netværk
- Opsætning og test af firewall regler
- Opsætning af usikre webservices 
- Dokumentation af det implementerede

### Materiale

## Mål for ugen

Herunder kan du læse ugens forskellige mål

### Praktiske mål

- Øvelse 25 
- Øvelse 54 


### Læringsmål der arbejdes med i faget denne uge

#### Overordnede læringsmål fra studie ordningen

- **_Den studerende har viden om og forståelse for_**
  - Gængse netværksenheder der bruges ifm. sikkerhed (firewall, IDS/IPS, honeypot, DPI)
- **_Den studerende kan_**
  - Identificere sårbarheder som et netværk kan have
- **_Den studerende kan håndtere udviklingsorienterede situationer herunder_**
  - Designe, konstruere og implementere samt teste et sikkert netværk

#### Læringsmål den studerende kan bruge til selvvurdering

- Den studerende kan arbejde struktureret med implementering, test og dokumentation af sikrede netværk
- Den studerende kan implementere firewall regler der sikrer adskillelse af netværkssegmenter

# Uge 12 - Logning: logs, netværkstrafik, graylog

## Emner

### Ugens emner er

- Wordlist skanning
- Implementering af docker vm host
- Opsætning af portainer på docker vm host
- Opsætning og konfiguration af graylog 
- Dokumentation af det implementerede

### Materiale

## Mål for ugen

Herunder kan du læse ugens forskellige mål

### Praktiske mål

- Øvelse 42 [Enumering med offensive værktøjer]
- Øvelse 80 [Virtuel Docker host] 
- Øvelse 81 [Portainer på Docker host] 
- Øvelse 82 [Graylog og firewall regler]
- Øvelse 83 [Graylog installation i docker via Portainer]
- Øvelse 84 [Graylog konfiguration] 


### Læringsmål der arbejdes med i faget denne uge

#### Overordnede læringsmål fra studie ordningen

- **_Den studerende har viden om og forståelse for_**
  - Netværkstrusler
  - Forskellige sniffing strategier og teknikker
  - Netværk management (overvågning/logning, snmp)
- **_Den studerende kan_**
  - Overvåge netværk samt netværkskomponenter
  - Teste netværk for angreb rettet mod de mest anvendte protokoller
- **_Den studerende kan håndtere udviklingsorienterede situationer herunder_**
  - Designe, konstruere og implementere samt teste et sikkert netværk
  - Monitorere og administrere et netværks komponenter

#### Læringsmål den studerende kan bruge til selvvurdering

- Den studerende har en forståelse for hvordan logning af netværks data kan udføres
- Den studerende kan opsætte og konfigurere et logopsamlingssystem
- Den studerende kan udføre wordlist skanninger med henblik på at afprøve logopsamlingssystemet